import scipy.constants as constants
from astropy import constants as Aconstants

c_SI = constants.speed_of_light
e_SI = constants.elementary_charge
h_SI = constants.Planck
hbar_SI = constants.hbar
ε0_SI = constants.epsilon_0

# Gaussian units
c = Aconstants.c.cgs.value
e = Aconstants.e.gauss.value
h = Aconstants.h.cgs.value
hbar = Aconstants.hbar.cgs.value
ε0 = 1
