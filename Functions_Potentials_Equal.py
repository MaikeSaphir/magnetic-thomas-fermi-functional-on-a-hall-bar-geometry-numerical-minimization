## Definition of Potentials that are independent of the unit system
## Used mostly in Functions_Potentials_Gaussian
import numpy as np
import scipy.integrate as integrate

def K1(tpd, xpd):
    '''
    tpd = t/d with t column vector
    xpd = x/d with x row vector
    '''
    return np.log(np.abs(np.sqrt((1-tpd**2)*(1-xpd**2))+1-tpd*xpd))

def K2(tpd, xpd):
    '''
    tpd = t/d with t column vector
    xpd = x/d with x row vector
    '''
    return np.log(np.abs(xpd - tpd))

def V_K1_simps(xpd, ρ):
    '''
    Simpson integration for K₁(t,x) for x in {-1+δ/d, ..., 1-δ/d}
    Since for x ϵ {-1, +1}: K(t,x) = K₁(t,x) - K₂(t,x) = 0 (equivalent for t ϵ {-1, 1}), but K₁(t,x) = ∞ for x = t = ± 1
    
    xpd : row vector
    '''
    x = xpd[:,1:-1]
    t = xpd.transpose() # t column vector in {-1,...,1}
    VK1x = integrate.simps(ρ.transpose()*K1(t, x), t, axis=0) # (N rows, N-2 columns)
    return VK1x.reshape(1,-1)

def V_K2_1δ_lin(x, ρ, δ):
    '''
    Integration of K₂(t,x), which is unbounded, but integrable; 
    The intervall is seperated in 1δ segments and ρ is approximated up to linear order.

    x: for V_K2 x/d (then also δ → δ/d); for ln|x-y| just x;
        x rowvektor
    δ: for V_K2 δ/d; for ln|x-y| just δ
    ρ: rowvektor as x
    '''
    ρ_δp2 = (ρ[:,1:] + ρ[:,:-1])/2
    dρ_δp2 = (ρ[:,1:] - ρ[:,:-1])/δ
    y = x.transpose()
    yDx = y - x
    
    maskEx = np.identity(yDx.shape[0], bool)
    # yDx is zero on the diagonal, so we can not calculate the logarithm.
    # When evaluating the integral, only terms of xⁿln(x-y) appear, and these evaluate to zero.
    LnYX = np.zeros_like(yDx)
    LnYX[~maskEx] = np.log(np.abs(yDx[~maskEx]))
    # LnYX = np.log(np.abs(yDx))
    # mask = np.isinf(LnYX)
    # maskEx = np.identity(yDx.shape[0], bool)
    # if np.sum(mask != maskEx) !=0:
    #     print('ln|y - x| is infinite beside the expected diagonal.')
    #     print(mask)
    F0 = np.zeros_like(yDx)
    F0[~maskEx] = yDx[~maskEx]*(LnYX[~maskEx] - 1)
    Int0 = (F0[1:] - F0[:-1]) # prefactor of d in V_K
    F1 = np.zeros_like(yDx)
    F1[~maskEx] = (yDx[~maskEx]**2)*(LnYX[~maskEx] - 0.5)
    Int1 = (0.5*(F1[1:] - F1[:-1])) # d² included in V_K and in δ/d in the derivative of ρ
    
    a0 = ρ_δp2.transpose() + dρ_δp2.transpose()*(-δ/2-yDx[:-1]) # Multiplication is elementwise
    
    Vx = (a0*Int0).sum(axis=0, keepdims=True) + np.matmul(dρ_δp2, Int1)
    return Vx

def V_external(xpd, V_ext):
    '''
    Electric potential created by external gates

    Calculated as equation 2 in the Review of Gerhardts.
    Here it should suffice to assume V_R = 0 and set V_L = V_ext.
    Note, V_ext has unit Joule, so we expect the factor of -e to be already included.
    '''
    return V_ext/2 - V_ext/np.pi*np.arcsin(xpd)
    # return np.linspace([0], [V_ext], num=x.shape[1], axis=1)
    
    
