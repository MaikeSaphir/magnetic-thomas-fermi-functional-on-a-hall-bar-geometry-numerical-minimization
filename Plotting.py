# Comments for someone aiming to work with this file:
# This is a file with code used to plot the results of the simulations and compare models with different parameters. I worked with an interactive Jupyter interface in VSCodium. Therefore, this code can't be used as a python script. 
# Actually, it may be easiest to integrate parts of this file in your own.

## Plotting in interactive python kernel
B = simulation0.B
ρ_last = model_one.simulations[B].ρ_last
plotHistory(model_one, B, legend=False)
plt.show() 
plotEnergyDensityPotentials(model_one, B)
plt.show()

ρ_last = model_one.simulations[0].ρ_last
plt.plot(model_one.x[0],pg.Ex(model_one, ρ_last, 0)[0])
plt.plot(model_one.x[0], np.ones_like(model_one.x[0])*simulation0.μ*np.trapz(ρ_last[0], dx=model_one.δ), ls='--', label='μ')
plt.show()

ρ_last = model_one.simulations[0].ρ_last
plt.plot(model_one.x[0],pg.gradE(model_one, ρ_last, 0)[0])
plt.plot(model_one.x[0], np.ones_like(model_one.x[0])*simulation0.μ, ls='--', label='μ')
plt.show()


# Plot several models
for model in models_dump.values():
  fig = mplt.plot_density_edge(model, 0, x_range=0.01)
  plt.show()

# Zoom near the edge and near ∇E
for B in model.simulations.keys():
    simulation = model.simulations[B]
    fig, ax = plt.subplots(1)
    ax = mplt.plot_potential_energies(model, model.simulations[B], ax, components=False)
    zoom = 0.05
    ax.set_ylim(top=simulation.μ*(1-zoom), bottom=simulation.μ*(1+zoom))
    ax.legend()
    plt.show()


# when ∇E is plotted in mplt.plot_density_edge
zoom = 1e-7
fig = mplt.plot_density_edge(model_one, 0, x_range=0.05, ylim= {'top': simulation0.μ/E0 + zoom, 'bottom': simulation0.μ/E0 - zoom}, right=False)
plt.show()


# Zoom into plot of ∇E in the middle
fig, ax1 = plt.subplots(1,1, layout='constrained')
ax1 = mplt.plot_potential_energies(model_one, simulation0, ax1)
ax1.set_xlim(left=-0.1, right=0.1)
zoom = 1e-7
ax1.set_ylim(top=simulation0.μ * (1 - zoom), bottom=simulation0.μ* (1 + zoom))
plt.legend()
plt.show()

# Compare ρ_last to ρ_{E min}
for key, simulation in model.simulations.items():
  fig, ax = plt.subplots(1, layout='constrained')
  ax = mplt.plot_density(model, simulation, ax, label=r'$\rho_{last}$', CSG=True, CMS=True)
  minE = np.argmin(simulation.func_history)
  ax.plot(model.xpd[0], simulation.ρ_history[minE], label=r'$\rho_{E min}$')
  plt.legend()
  plt.show()

  for key, simulation in model.simulations.items():
    fig, ax = plt.subplots(1, layout='constrained')
    minE = np.argmin(simulation.func_history)
    ax.plot(model.xpd[0], simulation.ρ_history[minE] - simulation.ρ_last[0], label=r'$\rho_{E min} - \rho_{last}$')
    plt.legend()
    plt.show()


# Find peaks in E_history and plot the related densities
simulation = simulation0
from scipy.signal import find_peaks
E_max_I, _ = find_peaks(simulation.func_history[:,0], distance=5)
E_min_I, _ = find_peaks(- simulation.func_history[:,0], distance=5)

for i in E_max_I:
  rho = simulation.ρ_history[i]
  plt.plot(model.x[0], rho, label=i)
plt.legend()
plt.show()

for i in E_min_I:
  rho = simulation.ρ_history[i]
  plt.plot(model.x[0], rho, label=i)
plt.legend()
plt.show()

########################################################################################################################
# Code for the plots produced
from importlib import reload
mplt = reload(mplt)

B_conv_SI = 1e-4 # from G to T: T = 10⁴G
V_conv_SI = 1e-7/e_SI # from erg to eV; eV = {e} C
########################################################################################################################
# Several B values
filename = 'Chemical_Pot-μ_09-B_8_values-further_iterated'
filename = 'Chemical_Pot-μ_09-iterated_once-B_10_values'
with open('Plots/'+filename+'.pkl', 'rb') as f:
    model = dill.load(f)
# Real filling factors
# for B, simulation in model.simulations.items():
#   simulation.edge_points()
#   print(B, ': ν = ',simulation.ρ_last[0,int(simulation.xm_i)]/model.dL(B))

# Figures to introduce the results, especially the potentials. Start with B=0:
fig = mplt.plot_energy_over_iterations_fig(model, 0)
filename_saving = filename + '-energy_over_iterations'
fig = mplt.plot_density_potentials(model, 0)
filename_saving = filename + '-density_potential_components_gradient'
fig.savefig('Plots/' + filename_saving + '.pdf')
fig.savefig('../Thesis/Figures/' + filename_saving + '.pgf')

# Figure to introduce the Incompressible stripes
B = 35191.34874921007 # list(model.simulations.keys())[-1]
# B = list(model.simulations.keys())[3]; Vtot_LandauL=2, y_zoom=3e-2
# B = list(model.simulations.keys())[4]; Vtot_LandauL=2, y_zoom=2e-2
filename_saving = filename + f'-Incompressible_srips-B{B*B_conv_SI:3.2f}'

fig = mplt.plot_density_potentials(model, B, Vtot_LandauL=3, y_zoom=2e-2)
filename_saving = filename_saving + '-density_potential_components_gradient'
fig.savefig('Plots/' + filename_saving + '.pdf')
fig.savefig('../Thesis/Figures/' + filename_saving + '.pgf')

fig = mplt.plot_energy_over_iterations_fig(model, B)
filename_saving = filename_saving + '-energy_over_iterations'

# All B values filling factor (left) and density (right)
for B in [list(model_copy.simulations.keys())[i] for i in [2, 7]]:
  model_copy.simulations.pop(B)

fig = mplt.plot_density_fillingfactor(model_copy, title=False, subfig_label='B_10_values-8_chosen')
filename_saving = filename + '-filling_factor'

fig = mplt.plot_density_fillingfactor(model, title=False, subfig_label='B_10_values')
filename_saving = filename + '-filling_factor-all_10'


########################################################################################################################
# filename = 'Vext_01-Chemical_Pot-μ_09'
filename = 'Chemical_Pot-μ_09-Vext_01E0'
# filename = 'Chemical_Pot-μ_09-iterated_once-B_10_values'
with open('Plots/'+filename+'.pkl', 'rb') as f:
    model_Vext = dill.load(f)
    # model_Bs = dill.load(f)

models_Vext = {0: model_Bs, 0.1: model_Vext}
B = list(model_Bs.simulations.keys())[4]
fig = mplt.plot_Vext_densities_potentials(models_Vext, B, y_zoom=2e-2)
########################################################################################################################
# Several μ to compare with Gerhardts, CSG, CMG and LG
filename = 'Edges-8-9-compare-Gerhardts'
with open('Plots/'+filename+'.pkl', 'rb') as f:
    models = dill.load(f)

models_plot = {}
for key, value in [list(models.items())[i] for i in [5,2,0]]:
  models_plot[key] = value
fig = mplt.plot_densities_pot_energies_compare(models_plot)
plt.show()
filename_short = filename + '-short'
fig.savefig('Plots/' + filename_short + '.pdf')
fig.savefig('../Thesis/Figures/' + filename_short + '.pgf')

models_plot = {}
for key, value in [list(models.items())[i] for i in [5,2,0,3,4]]:
  models_plot[key] = value
fig = mplt.plot_densities_edge_compare(models_plot, 0, x_range=0.08)
plt.show()
filename = filename + '-edge'
fig.savefig('Plots/' + filename + '.pdf', bbox_inches='tight')
fig.savefig('../Thesis/Figures/' + filename + '.pgf', bbox_inches='tight')

filename='Edges-8-9-compare-CMS-classical_case'
# ν = 0.9
# ρ_last = simulation.ρ_last
# dν = ρ_last[0,model.i_mid]/ν
# B = c*h/(2*e) * dν
model_cc = list(models_plot.values())[0]
B = list(model_cc.simulations.keys())[1]
fig = mplt.plot_densities_edge_compare_classic(models_plot, B, xlim={'left':-1.025, 'right': 0}, inset=True, x_range=0.06)
fig.savefig('Plots/' + filename + '.pdf', bbox_inches='tight')
fig.savefig('../Thesis/Figures/' + filename + '.pgf', bbox_inches='tight')

# fig = mplt.plot_densities_edge_compare_classic(models_plot, B, xlim={'left':-1.025, 'right': 0})

fig, ax1 = plt.subplots(1, figsize = (9, 5.6), layout='constrained')
for model in models_B.values():
  simulationB = model.simulations[B]
  simulation0 = model.simulations[0]
  ax1.plot(model.xpd[0], simulationB.ρ_last[0] - simulation0.ρ_last[0])
  print((simulationB.ρ_last[0] - simulation0.ρ_last[0])[model.i_mid])
plt.show()

for model in models_B.values():
  for B, simulation in model.simulations.items():
    print(B, model.x[0, simulation.xl_i]/model.d, model.x[0, simulation.xr_i]/model.d)
  print()

########################################################################################################################
########################################################################################################################
