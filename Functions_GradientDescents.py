# Different variations of Gradient Descent
import numpy as np

class Simulations:# BaseGradientDescents:
    def __init__(self, model, **simulationParas):
        for necessary_key in ['B']:
            if necessary_key not in simulationParas.keys():
                print('The necessary simulation parameter', necessary_key, 'is missing.')
        self.__dict__.update(simulationParas) # updates the list of attributes of the self. In case of overlapping keys the value of the latter dictionary is taken.
        model.simulations[simulationParas['B']] = self
    
    def gradient_descent_adj_chemicalpot(self, model, max_iterations, threshold, w_init, learning_rate=0.05, adjust_learning_rate=1, momentum=0.8, adjust_momentum=1):
        '''
        Gradient descent with chemical potential to constrain the number of electrons, so it is not held constant, but fixed by μ.
        Besides with the negative values for the densities are set to zero in every iteration step.
        % with smoothing of the density in every step over 2l_B + 1 gridpoints
        '''
        w = w_init
        # lB_gridpoints = int(np.round(1/model.δ*self.lB/2 - 0.5)) # Calculate the number of gridpoints over wich is smoothed, take into account the width of the middle gridpoint
        # smooth_arr = np.ones(lB_gridpoints + 1)
        # smooth_arr[[0,-1]] = 0.5 # For trapezoidal integration the first and last term have a factor of 1/2 in the sum
        w_history = w
        f_history = model.obj_func(w, self.B) - self.μ*np.trapz(w, dx=model.δ)
        delta_w = np.zeros(w.shape)
        i = 0
        diff = 1.0e10
        
        while i<max_iterations and diff>threshold:
            delta_w = -learning_rate*(model.grad_func(w, self.B) - self.μ) + momentum*delta_w
            w = w + delta_w
            # w has to take non-negative values, therefore set all negative values to zero
            w = np.maximum(0,w)
            # Alternatively, modulate delta_w or the gradient.
            
            # w = 1/(lB_gridpoints + 1) * np.convolve(w[0], smooth_arr, mode='same').reshape(1,-1)
            
            # store the history of w and f
            w_history = np.vstack((w_history,w))
            f_history = np.vstack((f_history, model.obj_func(w, self.B) - self.μ*np.trapz(w, dx=model.δ)))
            
            # update iteration number and diff between successive values
            # of objective function
            i+=1
            learning_rate = learning_rate*adjust_learning_rate
            momentum = momentum*adjust_momentum
            diff = np.absolute(f_history[-1] - f_history[-2])
            
            if i%5 == 0:
                print(i) if i%150 == 0 else print(i, end=' ')
                # print(i, ' ', model.grad_func(w, self.B)) if i%150 == 0 else print(i, ' ', model.grad_func(w, self.B), end=' ')
        
        self.simMethod = 'Gradient Descent (adj, chemicalpot)'
        self.threshold = threshold
        self.ρ_init = w_init
        self.learning_rate = learning_rate
        self.adjust_learning_rate = adjust_learning_rate
        self.momentum = momentum 
        self.adjust_momentum = adjust_momentum
        
        self.ρ_history = w_history
        self.func_history = f_history
        self.iterations = i
        self.ρ_last = w
        # self.difference = diff
    
    def gradient_descent_adj_smooth_trap(self, model, max_iterations, threshold, w_init, learning_rate=0.05, adjust_learning_rate=1, momentum=0.8):
        ''' Gradient descent with smoothing of the density in every step over 2l_B + 1 gridpoints and with normalisation (with trapezoidal integration) to keep the number of electrons constant.'''
        w = w_init
        w_tot = 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum() # multiplication with δ cancels in the normalisation
        lB_gridpoints = int(np.round(1/model.δ*self.lB/2 - 0.5)) # Calculate the number of gridpoints over wich is smoothed, take into account the width of the middle gridpoint
        # smoothed_length = (lB_gridpoints + 1)*δ # Length over which is smoothed
        smooth_arr = np.ones(lB_gridpoints + 1)
        smooth_arr[[0,-1]] = 0.5 # For trapezoidal integration the first and last term have a factor of 1/2 in the sum
        w_history = w
        f_history =  model.obj_func(w, self.B)
        delta_w = np.zeros(w.shape)
        i = 0
        diff = 1.0e10
        
        while i<max_iterations and diff>threshold:
            delta_w = -learning_rate*model.grad_func(w, self.B) + momentum*delta_w
            w = w + delta_w
            w = 1/(lB_gridpoints + 1) * np.convolve(w[0], smooth_arr, mode='same').reshape(1,-1)
            w = w_tot/(0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum()) * w
            
            # store the history of w and f
            w_history = np.vstack((w_history,w))
            f_history = np.vstack((f_history, model.obj_func(w, self.B)))
            
            # update iteration number and diff between successive values
            # of objective function
            i+=1
            learning_rate = learning_rate*adjust_learning_rate
            diff = np.absolute(f_history[-1] - f_history[-2])
            
            if i%5 == 0:
                print(i) if i%150 == 0 else print(i, end=' ')
        
        self.simMethod = 'Gradient Descent (adj, smooth, trap)'
        self.threshold = threshold
        self.ρ_init = w_init
        self.learning_rate = learning_rate
        self.adjust_learning_rate = adjust_learning_rate
        self.momentum = momentum 
        
        self.ρ_history = w_history
        self.func_history = f_history
        self.iterations = i
        self.ρ_last = w
        # self.difference = diff

    def gradient_descent_adj_trap(self, model, max_iterations, threshold, w_init, learning_rate=0.05, adjust_learning_rate=1, momentum=0.8):
        ''' Gradient descent with normalisation (with trapezoidal integration) to keep the number of electrons constant.'''
        w = w_init
        w_tot = 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum() # multiplication with δ cancels in the normalisation
        w_history = w
        f_history = model.obj_func(w, self.B)
        delta_w = np.zeros(w.shape)
        i = 0
        diff = 1.0e10
        
        while i<max_iterations and diff>threshold:
            delta_w = -learning_rate*model.grad_func(w, self.B) + momentum*delta_w
            w = w + delta_w
            w = w_tot/(0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum()) * w
            
            # store the history of w and f
            w_history = np.vstack((w_history,w))
            f_history = np.vstack((f_history, model.obj_func(w, self.B)))
            
            # update iteration number and diff between successive values
            # of objective function
            i+=1
            learning_rate = learning_rate*adjust_learning_rate
            diff = np.absolute(f_history[-1] - f_history[-2])
            
            if i%5 == 0:
                print(i) if i%150 == 0 else print(i, end=' ')
        
        self.simMethod = 'Gradient Descent (adj, trap)'
        self.threshold = threshold
        self.ρ_init = w_init
        self.learning_rate = learning_rate
        self.adjust_learning_rate = adjust_learning_rate
        self.momentum = momentum 
        
        self.ρ_history = w_history
        self.func_history = f_history
        self.iterations = i
        self.ρ_last = w

    def gradient_descent_adj_shifted_grad(self, model, max_iterations, threshold, w_init, learning_rate=0.05, adjust_learning_rate=1, momentum=0.8):
        ''' Gradient descent where the gradient is shifted (only inner grid points), such that the sum is zero, to keep the number of electrons constant. At the boundary points the gradient is left at zero, to satisfy the boundary condition.'''
        w = w_init
        print('w_sum: ', w.sum())
        print('w_tot: ', 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum())
        
        w_history = w
        f_history = model.obj_func(w, self.B)
        delta_w = np.zeros(w.shape)
        N = w.shape[1] - 2
        i = 0
        diff = 1.0e10
        while i<max_iterations and diff>threshold:
            gradient = model.grad_func(w, self.B)
            print('Before shift: Σ ∇E = ', gradient.sum(), gradient[:,[0,-1]])
            gradient[:,1:-1] = gradient[:,1:-1] - 1/N*gradient[:,1:-1].sum()
            print('After shift: Σ ∇E = ', gradient.sum(), gradient[:,[0,-1]])
            delta_w = -learning_rate*gradient + momentum*delta_w
            w = w + delta_w
            
            # store the history of w and f
            w_history = np.vstack((w_history,w))
            f_history = np.vstack((f_history, model.obj_func(w, self.B)))
            
            # update iteration number and diff between successive values
            # of objective function
            i+=1
            learning_rate = learning_rate*adjust_learning_rate
            diff = np.absolute(f_history[-1] - f_history[-2])
            
            if i%5 == 0:
                print(i) if i%150 == 0 else print(i, end=' ')
        
        print('w_sum: ', w.sum())
        print('w_tot: ', 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum())
        
        self.simMethod = 'Gradient Descent (adj, shifted grad)'
        self.threshold = threshold
        self.ρ_init = w_init
        self.learning_rate = learning_rate
        self.adjust_learning_rate = adjust_learning_rate
        self.momentum = momentum 
        
        self.ρ_history = w_history
        self.func_history = f_history
        self.iterations = i
        self.ρ_last = w

    def gradient_descent_adj_projected_grad(self, model, max_iterations, threshold, w_init, learning_rate=0.05, adjust_learning_rate=0.9, momentum=0.8):
        ''' Gradient descent where the gradient is projected to the plane, to keep the number of electrons constant.'''
        w = w_init
        print('w_sum: ', w.sum())
        print('w_tot: ', 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum())

        w_history = w
        f_history = model.obj_func(w, self.B)
        delta_w = np.zeros(w.shape)
        N = w.shape[1]
        i = 0
        diff = 1.0e10
        while i<max_iterations and diff>threshold:
            gradient = model.grad_func(w, self.B)
            e_w = w/w.sum()
            gradient = gradient - (gradient*e_w).sum()*e_w
            delta_w = -learning_rate*gradient + momentum*delta_w
            w = w + delta_w

            # store the history of w and f
            w_history = np.vstack((w_history,w))
            f_history = np.vstack((f_history,model.obj_func(w, self.B)))

            # update iteration number and diff between successive values
            # of objective function
            i+=1
            learning_rate = learning_rate*adjust_learning_rate
            diff = np.absolute(f_history[-1] - f_history[-2])

            if i%5 == 0:
                print(i) if i%150 == 0 else print(i, end=' ')

        print('w_sum: ', w.sum())
        print('w_tot: ', 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum())
        
        self.simMethod = 'Gradient Descent (adj, projected grad)'
        self.threshold = threshold
        self.ρ_init = w_init
        self.learning_rate = learning_rate
        self.adjust_learning_rate = adjust_learning_rate
        self.momentum = momentum 
        
        self.ρ_history = w_history
        self.func_history = f_history
        self.iterations = i
        self.ρ_last = w

    def gradient_descent_adj(self, model, max_iterations, threshold, w_init, learning_rate=0.05, momentum=0.8):
        w = w_init
        w_avg = np.mean(w)
        w_history = w
        delta_w = np.zeros(w.shape)
        i = 0
        diff = 1.0e10
        
        while i<max_iterations and diff>threshold:
            delta_w = -learning_rate*model.grad_func(w, self.B) + momentum*delta_w
            w = w + delta_w
            w = w_avg/np.mean(w) * w
            
            # store the history of w and f
            w_history = np.vstack((w_history,w))
            
            # update iteration number and diff between successive values
            # of objective function
            i+=1
            #learning_rate = learning_rate/2
            diff = np.max(np.absolute(w_history[-1] - w_history[-2]))
            if i%5 == 0:
                print(i) if i%150 == 0 else print(i, end=' ')
        
        self.simMethod = 'Gradient Descent (adj)'
        self.threshold = threshold
        self.ρ_init = w_init
        self.learning_rate = learning_rate
        self.adjust_learning_rate = adjust_learning_rate
        self.momentum = momentum 
        
        self.ρ_history = w_history
        self.func_history = f_history
        self.iterations = i
        self.ρ_last = w

    ## Analysis of results
    def edge_points(self):
        '''
        Searchs the indices where the final density of self is not zero, therefore the edges of the density.
        If found these are added as xl and xr to self attributes, otherwise the result is printed.

        The function does not check
        '''
        indices = np.nonzero(self.ρ_last[0])[0]
        if len(indices) >= 2:
            self.xl_i = indices[0]
            self.xr_i = indices[-1]
            self.xm_i = (self.xl_i + self.xr_i)/2
        else:
            print(f'The density is everywhere zero or only nonzero at {indices}. Good luck, checking out the problem.')


# def gradient_descent(self, model, max_iterations, threshold, w_init, obj_func, grad_func, extra_param=[], learning_rate=0.05, momentum=0.8):
    #     # From: https://stackabuse.com/gradient-descent-in-python-implementation-and-theory/
    #     w = w_init
    #     w_history = w
    #     f_history = obj_func(w,extra_param)
    #     delta_w = np.zeros(w.shape)
    #     i = 0
    #     diff = 1.0e10
        
    #     while  i<max_iterations and diff>threshold:
    #         delta_w = -learning_rate*grad_func(w,extra_param) + momentum*delta_w
    #         w = w+delta_w
            
    #         # store the history of w and f
    #         w_history = np.vstack((w_history,w))
    #         f_history = np.vstack((f_history,obj_func(w,extra_param)))
            
    #         # update iteration number and diff between successive values
    #         # of objective function
    #         i+=1
    #         diff = np.absolute(f_history[-1]-f_history[-2])
    #     return w_history,f_history