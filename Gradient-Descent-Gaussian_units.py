import numpy as np
import scipy.integrate as integrate
import scipy.constants as constants
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import datetime

import os
os.getcwd()

import sklearn.datasets as dt
from sklearn.model_selection import train_test_split


e = 1
c = 1
h = 1
hbar = 1
ε0 = 1

# From: https://stackabuse.com/gradient-descent-in-python-implementation-and-theory/
def gradient_descent(max_iterations, threshold, w_init, obj_func, grad_func, extra_param=[], learning_rate=0.05, momentum=0.8):
    w = w_init
    w_history = w
    f_history = obj_func(w,extra_param)
    delta_w = np.zeros(w.shape)
    i = 0
    diff = 1.0e10
    
    while  i<max_iterations and diff>threshold:
        delta_w = -learning_rate*grad_func(w,extra_param) + momentum*delta_w
        w = w+delta_w
        
        # store the history of w and f
        w_history = np.vstack((w_history,w))
        f_history = np.vstack((f_history,obj_func(w,extra_param)))
        
        # update iteration number and diff between successive values
        # of objective function
        i+=1
        diff = np.absolute(f_history[-1]-f_history[-2])
    return w_history,f_history


def gradient_descent_adj_trap(max_iterations, threshold, w_init, obj_func, grad_func, extra_param=[], learning_rate=0.05, adjust_learning_rate=0.9, momentum=0.8):
    w = w_init
    w_tot = 0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum() # multiplication with δ cancels normalisation
    w_history = w
    f_history = obj_func(w,*extra_param)
    delta_w = np.zeros(w.shape)
    i = 0
    diff = 1.0e10
    
    while  i<max_iterations and diff>threshold:
        delta_w = -learning_rate*grad_func(w,*extra_param) + momentum*delta_w
        w = w + delta_w
        w = w_tot/(0.5*(w[:,0] + w[:,-1]) + w[:,1:-1].sum()) * w
        
        # store the history of w and f
        w_history = np.vstack((w_history,w))
        f_history = np.vstack((f_history,obj_func(w,*extra_param)))
        
        # update iteration number and diff between successive values
        # of objective function
        i+=1
        learning_rate = learning_rate*adjust_learning_rate
        diff = np.absolute(f_history[-1] - f_history[-2])
        
        if i%5 == 0:
            print(i) if i%150 == 0 else print(i, end=' ')
    
    return w_history, f_history, i, diff

    
def gradient_descent_adj(max_iterations, threshold, w_init, grad_func, extra_param=[], learning_rate=0.05, momentum=0.8):
    w = w_init
    w_avg = np.mean(w)
    w_history = w
    delta_w = np.zeros(w.shape)
    i = 0
    diff = 1.0e10
    
    while  i<max_iterations and diff>threshold:
        delta_w = -learning_rate*grad_func(w,*extra_param) + momentum*delta_w
        w = w + delta_w
        w = w_avg/np.mean(w) * w
        
        # store the history of w and f
        w_history = np.vstack((w_history,w))
        
        # update iteration number and diff between successive values
        # of objective function
        i+=1
        #learning_rate = learning_rate/2
        diff = np.max(np.absolute(w_history[-1] - w_history[-2]))
        if i%5 == 0:
            print(i) if i%150 == 0 else print(i, end=' ')
    
    return w_history, i, diff

# def gradient_descent_adj_simps(max_iterations, threshold, w_init, x, grad_func, extra_param=[], learning_rate=0.05, momentum=0.8):
#     w = w_init
#     w_avg = np.mean(w)
#     w_history = w
#     delta_w = np.zeros(w.shape)
#     i = 0
#     diff = 1.0e10
#
#     while  i<max_iterations and diff>threshold:
#         delta_w = -learning_rate*grad_func(w,*extra_param) + momentum*delta_w
#         w = w + delta_w
#         w = w_avg/np.mean(w) * w
#
#         # store the history of w and f
#         w_history = np.vstack((w_history,w))
#
#         # update iteration number and diff between successive values
#         # of objective function
#         i+=1
#         learning_rate = learning_rate/2
#         diff = np.max(np.absolute(w_history[-1] - w_history[-2]))
#
#     return w_history, i ,diff

# delta_w
# w
# ρ
# djB(ρ, B)
# U_donor(x, d, n0, κ)/e
# V_K_1δ_lin(xpd, ρ, δpd, κ)
#
#
# gradient_descent_adj(1e4, 1e-10, ρ, gradE, extra_param=[x, d, B, n0, κ, δpd], learning_rate=0.5, momentum=0.8)
# max_iterations = 1e4
# threshold = 1e-5
# w_init = ρ
# grad_func = gradE
# extra_param=[x, d, B, n0, κ, xpd, δpd]
# learning_rate = 1
# momentum=0.8



def K1(tpd, xpd):
    '''
    tpd = t/d with t column vector
    xpd = x/d with x row vector
    '''
    return np.log(np.abs(np.sqrt((1-tpd**2)*(1-xpd**2))+1-tpd*xpd))

def K2(tpd, xpd):
    '''
    tpd = t/d with t column vector
    xpd = x/d with x row vector
    '''
    return np.log(np.abs(xpd - tpd))

def V_K1_simps(xpd, ρ):
    '''
    Simpson integration for K₁(t,x) for x/d in {-1+δ/d, ..., 1-δ/d}
    For x ϵ {-1, 1}: K(t,x) = K₁(t,x) + K₂(t,x) = 0
    
    xpd : row vector
    '''
    x = xpd[:,1:-1]
    t = xpd.transpose() # t column vector
    VK1x = integrate.simps(ρ.transpose()*K1(t, x), t, axis=0)
    return VK1x.reshape(1,-1)

def V_K2_1δ_lin(x, ρ, δ, d):
    '''
    x: for V_K2 x/d (then also δ → δ/d); for ln|x-y| just x;
        x rowvektor
    δ: for V_K2 δ/d; for ln|x-y| just δ
    d: for V_K2 d=model.d; for ln|x-y| d=1
    ρ: rowvektor as x
    '''
    ρ_δp2 = (ρ[:,1:] + ρ[:,:-1])/2
    dρ_δp2 = (ρ[:,1:] - ρ[:,:-1])/δ
    y = x.transpose()
    yDx = y - x
    
    maskEx = np.identity(yDx.shape[0], bool)
    # yDx is zero on the diagonal, so we can not calculate the logarithm.
    # When evaluating the integral, only terms of xⁿln(x-y) appear, and these evaluate to zero.
    LnYX = np.zeros_like(yDx)
    LnYX[~maskEx] = np.log(np.abs(yDx[~maskEx]))
    # LnYX = np.log(np.abs(yDx))
    # mask = np.isinf(LnYX)
    # maskEx = np.identity(yDx.shape[0], bool)
    # if np.sum(mask != maskEx) !=0:
    #     print('ln|y - x| is infinite beside the expected diagonal.')
    #     print(mask)
    F0 = np.zeros_like(yDx)
    F0[~maskEx] = yDx[~maskEx]*(LnYX[~maskEx] - 1)
    Int0 = d*(F0[1:] - F0[:-1])
    F1 = np.zeros_like(yDx)
    F1[~maskEx] = (yDx[~maskEx]**2)*(LnYX[~maskEx] - 0.5)
    Int1 = d**2*(0.5*(F1[1:] - F1[:-1]))
    
    a0 = ρ_δp2.transpose() + dρ_δp2.transpose()*(-δ/2-yDx[:-1]) # Multiplication is elementwise
    
    Vx = (a0*Int0).sum(axis=0, keepdims=True) + np.matmul(dρ_δp2, Int1)
    return Vx

def V_K_1δ_lin(xpd, ρ, δpd, κ, d):
    VKx = V_K2_1δ_lin(xpd, ρ, δpd. d)
    VKx[0,1:-1] = V_K1_simps(xpd, ρ) - VKx[0,1:-1]
    VKx[0,0] = 0
    VKx[0,-1] = 0
    return 2*e*d/κ*VKx

def potential(x):
    V = np.ones_like(x)
    return V

def V_eeInteraction(ρ, x, LnXY):
    Vee = 2*(x[1]-x[0])*np.matmul(ρ,LnXY)
    return Vee

def U_donor(x, d, n0, κ):
    E0 = 2*np.pi*e**2*n0*d/κ
    # E0 = 4.377 # in eV
    U_d = -E0 * np.sqrt(1-(x/d)**2) # in eV
    return U_d

# def dν(B, γ, ν):
#     if isinstance(γ, np.int):
#         if ν in np.arange(0,np.abs(γ)): # ν in {0,1,...,|γ|-1}
#             return B/(2*np.pi)
#         else:
#             return B/np.pi
#     else:
#         return B/(2*np.pi)

def jB(ρ, B, m):
    '''
    Attention ν_max here is equal to (ν_max,paper - 1). Here ν_max is the highest Landau level that is occupied, not necessarily filled.
    jB(ρ) = Σ_{ν=0}^{ν_max -1} ε_ν d_ν  +  (ρ - D_{ν_max - 1}) ε_{ν_max}
    '''
    if B == 0:
        return np.zeros_like(ρ)
    else:
        dν = (2*e*B)/(c*h)  # c*h/(2*e*B) in Gaussian units
        ν = ρ/dν
        ν_max = np.ceil(ν).max()
        hbar_ωc = hbar*e*B/(m*c)
        return hbar_ωc*(-0.5*ν_max**2*dν + (ν_max + 0.5)*ρ)

def djB(ρ, B, m):
    if B == 0:
        return np.zeros_like(ρ)
    else:
        dν = (2*e*B)/(c*h)  # c*h/(2*e*B) in Gaussian units
        ν = ρ/dν
        return hbar*e*B/(m*c)*(np.floor(ν) + 0.5)

def djB_smooth(ρ, B, m, k):
    '''
    djB is a sum of Heaviside functions. Approximate the Heaviside functions with 1/(1 + exp{-2kx}) = 1/2 + 1/2 tanh(kx) and calculate the derivative.
    The approximation approaches djB for k → ∞.
    '''
    if B == 0:
        return np.zeros_like(ρ)
    else:
        dν = (2*e*B)/(c*h)  # c*h/(2*e*B) in Gaussian units
        ν = ρ/dν
        ν_max = np.ceil(ν).max()
        djB = 0.5 + 0.5*np.tanh(k*(ν - np.arange(ν_max + 1).reshape(-1,1)))
        djB = hbar*e*B/(m*c)*(djB.sum(axis=0, keepdims=True) + 0.5)
        return djB

# def d2jB(ρ, B, m, k):
#     '''
#     Approximate the Heaviside functions in djB with 1/(1 + exp{-2kx}) and calculate the derivative.
#     The approximation approaches djB and d2jB for k → ∞.
#     '''
#     if B == 0:
#         return np.zeros_like(ρ)
#     else:
#         dν = c*h/(2*e*B)  # c*h/(2*e*B) in Gaussian units
#         ν = ρ/dν
#         ν_max = np.ceil(ν).max()
#         d2jB = 0.5/np.cosh(k*(ν - np.arange(ν_max + 1).reshape(-1,1)))**2
#         d2jB = hbar*e*B/(m*c)*k*d2jB.sum(axis=0, keepdims=True)
#         return d2jB

def gradE(ρ, x, d, B, m, n0, κ, δ, δpd):
    return djB(ρ, B, m) + U_donor(x, d, n0, κ)/e + V_K_1δ_lin(x/d, ρ, δpd, κ)

def gradE_smooth(ρ, x, d, B, m, n0, κ, δ, δpd, k):
    return djB_smooth(ρ, B, m, k) + U_donor(x, d, n0, κ)/e + V_K_1δ_lin(x/d, ρ, δpd, κ)

def Ex(ρ, x, d, B, m, n0, κ, δ, δpd):
    ''' E[ρ](x) Energy functional'''
    return jB(ρ, B, m) + ρ*(U_donor(x, d, n0, κ) + e*V_K_1δ_lin(x/d, ρ, δpd, κ))

def E(ρ, x, d, B, m, n0, κ, δ, δpd):
    '''
    Integration implemented with trapezoidal rule.
    integrand = Ex(ρ, x, d, B, m, n0, κ, δ, δpd)[0]
    integrand[0] = integrand[0]/2
    integrand[-1] = integrand[-1]/2
    δ * np.sum(integrand)
    '''
    return np.trapz(Ex(ρ, x, d, B, m, n0, κ, δ, δpd)[0], dx=δ)

def E_smooth(ρ, x, d, B, m, n0, κ, δ, δpd, k):
    return E(ρ, x, d, B, m, n0, κ, δ, δpd)

# djB(ρ, B)
# U_donor(x, d, n0, κ)/e
# V_K_1δ_lin(xpd, ρ, δpd, κ)
#
# gradE(ρ, x, d, B, n0, κ, xpd, δpd)




d = 1 # in m
δ = 0.001 # in m
N = int(2*d/δ) + 1
δ = 2*d/(N-1)

n0 = 10
ne0 = 0.5
E0 = 1
a0 = 1
κ = 2*np.pi*e**2*n0*d/E0
m = np.pi*n0*d*hbar**2/(a0*E0)



x = np.linspace(-d, d, N).reshape(1,-1)
y = x.transpose()
ρ = np.ones_like(x)*ne0
ρ[0,0] = 0
ρ[0,-1] = 0
xpd = x/d
δpd = δ/d


B = 1
ρ_history, iteration, diff = gradient_descent_adj(1e3, 1e-6, ρ, gradE, extra_param=[x, d, B, m, n0, κ, δpd], learning_rate=0.1, momentum=0.8)
iteration
diff


# Gradient Descent with Energy-Functional
B = 0
ρE_history, E_history, iteration, diff = gradient_descent_adj_trap(1e3, 1e-10, ρ, E, gradE, extra_param=[x, d, B, m, n0, κ, δ, δpd], learning_rate=1e10, adjust_learning_rate=1, momentum=0.9)
iteration
diff
plt.plot(E_history)

# Plot History of E[ρ](x)
for i, rho in enumerate(ρE_history[::50,:]):
    rho = rho.reshape(1,-1)
    plt.plot(x[0], Ex(rho, x, d, B, m, n0, κ, δ, δpd)[0], label=i)
    plt.legend()

plt.plot(x[0], Ex(ρ_last, x, d, B, m, n0, κ, δ, δpd)[0])
plt.plot(x[0], gradE(ρ_last, x, d, B, m, n0, κ, δ, δpd)[0])
plt.plot(x[0], gradE(ρ, x, d, B, m, n0, κ, δ, δpd)[0])


ρ_B0 = ρ
ρ = ρE_history[-1:]
ρ

B = 1
B = 1*1e4 # in G; T = 10⁴ G
ρE_history, E_history, iteration, diff = gradient_descent_adj_trap(1e3, 1e-10, ρ, E, gradE, extra_param=[x, d, B, m, n0, κ, δ, δpd], learning_rate=1e10, adjust_learning_rate=1, momentum=0.8)
iteration
diff
plt.plot(E_history)

# Calculation with smoothened djB
B = 1
k = 15
ρE_sm_history, E_sm_history, iteration, diff = gradient_descent_adj_trap(1e3, 1e-10, ρ, E_smooth, gradE_smooth, extra_param=[x, d, B, m, n0, κ, δ, δpd, k], learning_rate=0.1, momentum=0.8)
iteration
diff
plt.plot(E_sm_history)



# [0.8,0.9,1,1.1,1.2,1.5]
# np.linspace(1.5,1.8, 10)
for B in [0.8,0.9,1,1.1,1.2,1.5,2]:
    ρE_history, E_history, iteration, diff = gradient_descent_adj_trap(1e3, 1e-10, ρ, E, gradE, extra_param=[x, d, B, m, n0, κ, δ, δpd], learning_rate=0.1, momentum=0.8)
    #ρ_history, iteration, diff = gradient_descent_adj(1e4, 1e-15, ρ, E, gradE, extra_param=[x, d, B, n0, κ, δpd], learning_rate=0.5, momentum=0.8)
    
    fig, ax = plt.subplots()
    ax.plot(x[0], ρE_history[-1], label='ρ')
    ax.plot(x[0], np.ones_like(x[0])*n0, ls='--', label='n_d')
    # ax.plot(x[0], -U_donor(x, d, n0, κ)[0], label='-U_d')
    # ax.plot(x[0], V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='V_K')
    # ax.plot(x[0], djB(ρ_last, B, m)[0], label='dj/dρ')
    # ax.plot(x[0], -gradE(ρ_last, x, d, B, m, n0, κ, δ, δpd)[0], label='-∇E')
    #ax.plot(x[0], djB(ρ, B, m)[0] + V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='dj/dρ + V_K')
    ax.legend()
    ax.set_title(f'B = {B}T, Iteration = {iteration}')




# Save as pdf
with PdfPages('Density-Potential-B1-simple.pdf') as pdf:

    B = 0
    ρ_last = np.array([ρE_history[-1]])
    if True:
        fig, (ax1, ax2) = plt.subplots(2, figsize = (6.4, 9.6))
        #ax.plot(x[0], ρ_history[-1], label='ρ')
        ax1.plot(x[0], ρ_last[0], label='ρ')
        #ax.plot(x[0], np.ones_like(x[0])*n0, ls='--', label='n_d')
        ax2.plot(x[0], -U_donor(x, d, n0, κ)[0]/e, label='-U_d')
        ax2.plot(x[0], V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='eV_K')
        ax2.plot(x[0], djB(ρ_last, B, m)[0], label='dj/dρ')
        #ax2.plot(x[0], -gradE(ρ_last, x, d, B, m, n0, κ, δ, δpd)[0], label='-∇E')
        #ax2.plot(x[0], djB(ρ, B, m)[0] + V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='dj/dρ + V_K')
        ax1.legend()
        ax2.legend()
        ax1.set_title(f'B = {B}T, Iteration = {iteration}')

    pdf.savefig(fig)
    plt.close()

# Set metadata of Pdf-file
d = pdf.infodict()
#d['Title'] = 'Multipage PDF Example'
d['Author'] = 'Maike Gnirß'
#d['Subject'] = 'How to create a multipage pdf file and set its metadata'
#d['Keywords'] = 'PdfPages multipage keywords author title subject'
d['CreationDate'] = datetime.datetime(2022, 3, 8)
d['ModDate'] = datetime.datetime.today()

plt.rcParams['text.usetex'] = False



ρ_last = np.array([ρE_history[-1]])
fig, ax = plt.subplots()
#ax.plot(x[0], ρ_history[-1], label='ρ')
ax.plot(x[0], ρ_last[0], label='ρ')
ax.plot(x[0], np.ones_like(x[0])*n0, ls='--', label='n_d')
ax.plot(x[0], -U_donor(x, d, n0, κ)[0], label='-U_d')
ax.plot(x[0], -e*V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='-eV_K')
ax.plot(x[0], djB(ρ_last, B, m)[0], label='dj/dρ')
ax.plot(x[0], -gradE(ρ_last, x, d, B, m, n0, κ, δ, δpd)[0], label='-∇E')
#ax.plot(x[0], djB(ρ, B, m)[0] + V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='dj/dρ + V_K')
ax.legend()



# Plot smooth data
B = 1
ρ_last = np.array([ρE_sm_history[-1]])
fig, ax = plt.subplots()
#ax.plot(x[0], ρ_history[-1], label='ρ')
ax.plot(x[0], ρ_last[0], label='ρ')
ax.plot(x[0], np.ones_like(x[0])*n0, ls='--', label='n_d')
ax.plot(x[0], -U_donor(x, d, n0, κ)[0], label='-U_d')
ax.plot(x[0], V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='V_K')
ax.plot(x[0], djB_smooth(ρ_last, B, m, k)[0], label='dj/dρ')
ax.plot(x[0], gradE_smooth(ρ_last, x, d, B, m, n0, κ, δ, δpd, k)[0], label='∇E')
#ax.plot(x[0], djB(ρ, B, m)[0] + V_K_1δ_lin(x/d, ρ_last, δpd, κ)[0], label='dj/dρ + V_K')
ax.legend()
ax.set_title(f'B = {B}T, Iteration = {iteration}')

