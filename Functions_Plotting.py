# Functions to plot densities, potentials and energies
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.optimize import newton
from scipy.special import ellipe, ellipk

import Functions_GradientDescents as gd
import Functions_Potentials_Equal as pe
unit_sys = 'gaussian' # e.g.: ones, gaussian

# Function for sublabels (a, b,...) in each subplot
def _set_subfig_label(ax, label, vpos='top', hpos='right'):
    '''
    Setting sublabels (a, b, c) in each subplot.
    '''
    if hpos == 'right':
        text_xpos = 1
    elif hpos == 'left':
        text_xpos = 0
    else:
        raise ValueError(f'Argument hpos can only be `top` or `bottom` but not {hpos}.')
    
    if vpos == 'top':
        text_ypos = 1
    elif vpos == 'center':
        text_ypos = 0.5
    elif vpos == 'bottom':
        text_ypos = 0
    else:
        raise ValueError(f'Argument vpos can only be `top`, `center` or `bottom` but not {vpos}.')

    ax.text(text_xpos, text_ypos, f'\\subfigcaptionlabel{{{label}}}', ha=hpos, va=vpos, transform=ax.transAxes)

plt.Axes.set_subfig_label = _set_subfig_label


## -------------------------- Plotting specifications --------------------------
if unit_sys == 'ones':
    from Constants_Ones import * # better: import ... as consts
    import Functions_Potentials_Gaussian as pg # Should be changed to different file

    B_conv_SI = 1
    V_conv_SI = 1

    # formatter_energy_per_length = lambda y, pos: y
elif unit_sys == 'gaussian':
    from Constants_Gaussian import * # better: import ... as consts
    import Functions_Potentials_Gaussian as pg

    B_conv_SI = 1e-4 # from G to T: T = 10⁴G
    V_conv_SI = 1e-7/e_SI # from erg to eV; eV = {e} C V = {e} J = {e} 10⁷ erg
    # formatter_energy_per_length = lambda model: (lambda y, pos: r'${0:.3g}$'.format(y/model.E0))
    # label_energy_per_length = lambda label: r'${0} \,/\, (E_0 \, \unit{{\square\cm}})$'.format(label)

    def axis_energy_per_lengthPower(axes, model, label, lengthPower=0):
        '''
        Adds new axes to the plot, on the left side divided by E0 and on the right in SI-Units.
        lengthPower: Give the power of cm; either: 0,
        # lengthPower_unit: Give the unit in siunitx manner; either: '', '\per\cm' or '\per\square\cm'

        1 erg = 10⁻⁷/{e} eV; eV = {e} C V = {e} J = {e} 10⁷ erg
        1/cm = 10²/m
        '''
        ylabel_E0 = label + r'$\,/\,'
        ylabel_eV = label + r' in $\unit{\eV'
        if lengthPower == 0:
            ylabel_E0 += r'E_0$'
        elif lengthPower == 1:
            ylabel_E0 += r'(E_0 \, \unit{\per\cm})$'
            ylabel_eV += r'\per\cm'
        elif lengthPower == 2:
            ylabel_E0 += r'(E_0 \, \unit{\per\square\cm})$'
            ylabel_eV += r'\per\square\cm'
        else:
            raise ValueError('Unallowed value for lengthPower')
        ylabel_eV += r'}$'

        axes.get_yaxis().set_visible(False)
        secax_left = axes.secondary_yaxis('left', functions=(lambda x: x/model.E0, lambda x: x*model.E0))
        secax_left.set_ylabel(ylabel_E0)
        
        # Give axes a function as attribute, that allows to set the ylim later in new secondary axis coordinates.
        # Should be possible with some transform function of fig.get_axes()[i].child_axes.
        axes.set_ylim_E0_secaxis = lambda top, bottom: axes.set_ylim(top=top*model.E0, bottom=bottom*model.E0)

        secax_right = axes.secondary_yaxis('right', functions=(lambda x: x*1e-7/e_SI*(1e-2)**lengthPower, lambda x: x*1e7*e_SI*(1e-2)**lengthPower))
        secax_right.set_ylabel(ylabel_eV)

    #axis_energy_per_length = lambda axes, label: axes.secondary_yaxis('right', functions=(lambda x: 1e-5/e_SI*x, lambda x: 1e5*e_SI*x)).set_ylabel(r'${0}$ in $\unit{{\eV\per\cm}}$'.format(label))
else:
    print('No unit system choosen!')

## ------------------------------ Plot functions ------------------------------
# Only the plot functions with chemical potential are worked out in detail with Latex typesetting and so on
def plotTitleSimulation(model, simulation, fig):
    ''' Add title to figure fig. Use layout='constrained' in figure initialisation.'''
    if 'trap' in simulation.simMethod:
        fig.suptitle(f'Gradient Descent with fixed number of electrons ({simulation.simMethod})\nB = {B}T, V_ext = {model.V_ext}, Iterations = {simulation.iterations}')
    elif 'smooth' in simulation.simMethod:
        ## Smoothed density profiles
        fig.suptitle(f'Gradient Descent (smoothed, trap)\nB = {B}T, Iterations = {simulation.iterations}, lB = {simulation.lB}')
    elif 'projected' in simulation.simMethod:
        ## Density profiles with changing total number
        ρ_last_tot = np.trapz(simulation.ρ_last, dx=model_one.δ)
        ρ_init_tot = np.trapz(simulation.ρ_init, dx=model_one.δ)
        
        fig.suptitle(f'Gradient Descent (with projected gradient)\nB = {B}T, V_ext = {model.V_ext}, Iterations = {simulation.iterations}, ∫ρ = {ρ_last_tot[0]}, ∫ρ_init = {ρ_init_tot[0]}')
    elif 'chemicalpot' in simulation.simMethod:
        ## Density profiles where the total number is restricted by a chemical potential
        ρ_last_tot = np.trapz(simulation.ρ_last, dx=model.δ)[0]
        ρ_init_tot = np.trapz(simulation.ρ_init, dx=model.δ)[0]

        fig.suptitle(f'Gradient Descent with Chemical Potential ($\\mu = \\qty{{{simulation.μ*V_conv_SI:.3g}}}{{\J}}$)\n$B = \\qty{{{simulation.B*B_conv_SI:.3g}}}{{\\tesla}}$, $V_{{ext}} = \\qty{{{model.V_ext*V_conv_SI:.3g}}}{{\\J}}$, Iterations = \\num{{{simulation.iterations}}}, $\\int \\rho = \\qty{{{ρ_last_tot:.3g}}}{{\per\cm}}$, $\\int \\rho_{{init}} = \\qty{{{ρ_init_tot:.3g}}}{{\per\cm}}$')
    else:
        fig.suptitle(f'{simulation.simMethod}\nB = {simulation.B}T, V_ext = {model.V_ext}, Iterations = {simulation.iterations}, lB = {simulation.lB}')
    
    return fig

def plotTitleModel(model, fig):
    '''
    Add title to figure fig. Use layout='constrained' in figure initialisation.
    
    Attention for determing the specific Gradient Descent (simMethod) used, a random simulation from the model is taken, so make sure they all used the same.
    '''
    B = next(iter(model.simulations))
    simulation = model.simulations[B]
    if 'chemicalpot' in simulation.simMethod:
        ## Density profiles where the total number is restricted by a chemical potential
        fig.suptitle(f'Density and Filling Factor\nGradient Descent with Chemical Potential ($\\muG = \\num{{{model.μ_G/model.E0:.3g}}}\,E_0$), $V_{{ext}} = \\qty{{{model.V_ext*V_conv_SI:.3g}}}{{\\J}}$')
    else:
        fig.suptitle(f'Density and Filling Factor\n{model.simulation[B].simMethod}, $V_{{ext}} = \\qty{{{model.V_ext*V_conv_SI:.3g}}}{{\\J}}$')
    
    return fig
    
def plot_density(model, simulation, ax, donors=True, label=r'$\rho$', CSG=False, CMS=False, LG=False, plot_kwargs={}):
    '''
    Plots density of model (and donor density if donors is True) onto given axes ax, and returns ax.
    '''
    ax.plot(model.xpd[0], simulation.ρ_last[0], label=label, **plot_kwargs)
    if donors:
        ax.plot(model.xpd[0], np.ones_like(model.x[0])*model.n0, ls='-.', label=r'$\rho_d$')
    if CSG:
        ax = plot_CSG_density(model, simulation, ax, n0_ρ=model.n0, LG=LG)  
    if CMS:
        ax = plot_CMS_density(model, simulation, ax, n0_ρ=model.n0)    
    ax.set_xlabel(r'$x/d$')
    ax.set_ylabel(r'density in \(\unit{\per\square\cm}\)')
    return ax

def plot_fillingfactor(model, simulation, ax, label=None, plot_kwargs={}):
    '''
    If B is not zero, plots filling factor of model onto given axes ax, and returns ax. (For B =0 the energy is continuous, so there are no Landau levels.)
    
    label = r'\qty{{{:.3g}}}{{\tesla}}'.format(B*B_conv_SI)
    '''
    B = simulation.B
    if B == 0:
        line, = ax.plot([])
        return ax, line
        
    line, = ax.plot(model.xpd[0], simulation.ρ_last[0]/model.dL(B), label=label, **plot_kwargs)
    ax.set_xlabel(r'$x/d$')
    ax.set_ylabel(r'filling factor $\nu = \rho/\dL$')
    return ax, line

def plot_total_energy_density(model, simulation, ax):
    '''
    Plots total energy density (i.e. the value of the Hall functional) of the model over the iterations onto given axes ax, and returns ax.
    '''
    ax.plot(simulation.func_history, label=r'$\cEHall(\rho_i)$')
    ax.set_xlabel(r'Iteration \(i\)')
    axis_energy_per_lengthPower(ax, model, r'$\cEHall(\rho_i)$', lengthPower=1)
    # ax1.yaxis.set_major_formatter(formatter_energy_per_length(model))
    # ax1.set_ylabel(label_energy_per_length(r'E(\rho_i)'))
    # #ax1.get_yaxis().set_visible(False)
    # axis_eV_per_cm2(ax1, r'E(\rho_I)')
    return ax

def plot_potential_energies(model, simulation, ax, components=True):
    '''
    Plots the gradient of Hall functional (cE_Hall) of the model and if components is true also each potential energy onto given axes ax, and returns ax.
    ''' 
    ax.plot(model.xpd[0], model.grad_func(simulation.ρ_last, simulation.B)[0], label=r"$j'_B\circ\rho + \Vsub{tot}$") # label=r'$\nabla \cEHall(\rho)$'
    if 'chemicalpot' in simulation.simMethod:
        ax.plot(model.xpd[0], np.ones_like(model.x[0])*simulation.μ, ls=(0,(5,7)), c='C3', label=r'$\mu$')

    if components:
        ax.plot(model.xpd[0], -model.V_K_1δ_lin(simulation.ρ_last)[0], c='C4', label=r'$-\Vsub{H}$')
        ax.plot(model.xpd[0], model.V_donor[0], c='C5', label=r'$\Vsub{D}$')
        ax.plot(model.xpd[0], model.V_external[0], c='C2', label=r'$\Vsub{G}$')
        ax.plot(model.xpd[0], model.djB(simulation.ρ_last, simulation.B)[0], c='C1', ls='None', marker='o', markersize=0.93, markeredgewidth=0, label=r"$j'_B\circ\rho$") # ls='None', marker='.', markersize=0.1,
    # else:
    #     for i in range(4):
    #         next(ax._get_lines.prop_cycler)
    # next(ax._get_lines.prop_cycler)

    axis_energy_per_lengthPower(ax, model, r'potential energy $V$', lengthPower=0)
    ax.set_xlabel(r'$x/d$')
    return ax

def plot_tot_potential_Landau_levels(model, simulation, ax, Landau_levels=2, y_zoom=2e-2):
    '''
    Plots the the total potential (V_tot = V_D + V_H + V_ext) of the model and the chemical potential onto given axes ax, and returns ax.
    With Landau_levels one can give the number of energy levels (λ_n = V_tot + n ℏω̜_c ) to plot. Note, that only for B≠0, the energy levels are discrete and giving the energy levels makes sense.
    # if B = 0 (λ_n = V_tot + j'_B = ∇E(ρ)) is depicted.
    '''
    ax.plot(model.xpd[0], model.djB(simulation.ρ_last, simulation.B)[0] + simulation.μ, label=r"$j'_B \circ \rho + \mu$", c='C1', ls='None', marker='o', markersize=0.93, markeredgewidth=0)

    V_tot = model.V_donor + model.V_K_1δ_lin(simulation.ρ_last) + model.V_external
    if Landau_levels==0:
        line, = ax.plot(model.xpd[0], V_tot[0], label=r'$\Vsub{tot}$')
    else:
        if simulation.B == 0:
            print('Plotting the energy levels for B=0 does not make sense, since the kinetic energy spectrum is not discrete.')
            # line, = ax.plot(model.xpd[0], V_tot[0], label=r'$\Vsub{tot}$')
            # ax.plot(model.xpd[0], V_tot[0] + model.djB(simulation.ρ_last, simulation.B)[0], color=line.get_color(), ls='..', label=r"$\Vsub{tot} + j'_B\circ\rho = ∇\cEHall(\rho)$")
        else:
            line, = ax.plot(model.xpd[0], V_tot[0], label=r'$\Vsub{tot} = \lambda_0$')
            for n in range(1,Landau_levels):
                ax.plot(model.xpd[0], V_tot[0] + n*model.hbar_ωc(simulation.B), color=line.get_color(), ls=':', label=r'$\lambda_{{{}}}$'.format(n))

    if 'chemicalpot' in simulation.simMethod:
        ax.plot(model.xpd[0], np.ones_like(model.x[0])*simulation.μ, ls=(0,(5,7)), c='C3', label=r'$\mu$')

    axis_energy_per_lengthPower(ax, model, r'potential energy $V$', lengthPower=0)
    ax.set_xlabel(r'$x/d$')
    ax.set_ylim(top=simulation.μ * (1 - y_zoom), bottom=simulation.μ* (1 + y_zoom))
    return ax


def plot_energy_densities_x(model, simulation, ax, components=True):
    '''
    Plots energy density of model (and each contribution if components is True) onto given axes ax, and returns ax.
    ''' 
    if components:
        ax.plot(model.xpd[0], model.jB(simulation.ρ_last, simulation.B)[0], label=r'$j_B(\rho)$')
        ax.plot(model.xpd[0], (simulation.ρ_last*(model.V_donor + 0.5*model.V_K_1δ_lin(simulation.ρ_last)))[0], label=r'$\rho \,(\Vsub{D} + 1/2\, \Vsub{H})$')
        ax.plot(model.xpd[0], (simulation.ρ_last*model.V_external)[0], label=r'$\rho \,\Vsub{G}$')
        next(ax._get_lines.prop_cycler)
    ax.plot(model.xpd[0], pg.Ex(model, simulation.ρ_last, simulation.B)[0], label=r'$E(x)$')
    if 'chemicalpot' in simulation.simMethod:
        ax.plot(model.xpd[0], np.ones_like(model.x[0])*simulation.μ * np.trapz(simulation.ρ_last, dx=model.δ), ls='--', label=r'$\mu \,\int \rho$')
    
    ax.set_xlabel(r'$x/d$')
    axis_energy_per_lengthPower(ax, model, r'energy per area: $E$', lengthPower=2)

    return ax


def plotEnergyDensityPotentials(model, B):
    '''
    Figure with four subplots to show the results of the simulation for magnetic field B;
    i.e. plotted are the convergence of the total energy density,
    the resulting density, the potential energies and the components of the Hall functional resolved spatially.
    Returns the figure.
    '''
    simulation = model.simulations[B]
    #fig, (ax1, ax2, ax3) = plt.subplots(3, figsize = (5.7, 10.569))
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, figsize = (5.7, 14.092), layout='constrained')
    
    ax1 = plot_total_energy_density(model, simulation, ax1)
    ax2 = plot_density(model, simulation, ax2)
    ax3 = plot_potential_energies(model, simulation, ax3)
    ax4 = plot_energy_densities_x(model, simulation, ax4)
    
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()

    fig = plotTitleSimulation(model, simulation, fig)
    
    return fig # , (ax1, ax2, ax3)


def plot_density_edge(model, B, x_range=0.05, right=True, ylim=None):
    '''
    Plots density and potential energies of model around the left (and right) edge and returns the figure. To get a closer view, define by x_range how many percent of the full width 2d, should be plotted. Set ylim = {'top': top_lim/None, 'bottom': bottom_lim/None} by hand (because autoscaling is complicated).
    '''
    simulation = model.simulations[B]
    x_range_i = np.rint(x_range*2*model.d/model.δ).astype(int)

    if right:
        fig, [[ax1, ax3], [ax2, ax4]] = plt.subplots(2,2, figsize = (12, 13.5), layout='constrained', sharex='col')
    else:
        fig, [ax1, ax2] = plt.subplots(2,1, figsize = (12, 13.5), layout='constrained', sharex='col')

    ax1 = plot_density(model, simulation, ax1, donors=False, CSG=True, CMS=True, LG=True)
    ax1.label_outer()
    # ax1.xaxis.label.set_visible(False)
    ax1.legend()

    # ax2 = plot_potential_energies(model, simulation, ax2, components=True)
    ax2 = plot_energy_densities_x(model, simulation, ax2, components=False)
    ax2.set_xlim(left=model.xpd[0,max(simulation.xl_i - x_range_i, 0)], right=model.xpd[0,simulation.xl_i + x_range_i])
    if ylim != None:
        ax2.set_ylim_E0_secaxis(**ylim)
    ax2.legend()
    # visible_y = y[xmin < x < xmax]
    # if len(visible_y):
    #     ax.set_ylim(np.min(visible_y), np.max(visible_y)

    if right:
        ax3 = plot_density(model, simulation, ax3, donors=False, CSG=True, CMS=True)
        ax3.xaxis.label.set_visible(False)
        ax3.legend()

        ax4.set_xlim(left=model.xpd[0,simulation.xr_i - x_range_i], right=model.xpd[0,min(simulation.xr_i + x_range_i, model.xpd.shape[1]-1)])
        # ax4 = plot_potential_energies(model, simulation, ax4, components=True)
        ax4 = plot_energy_densities_x(model, simulation, ax4, components=True)
        if ylim != None:
            ax4.set_ylim_E0_secaxis(**ylim)
        ax4.legend()

    return fig


def plotHistory(model, B, legend=True):
    '''
    Plot History of E[ρ](x) and ρ(x) and show the figure.
    '''
    simulation = model.simulations[B]
    for i, rho in enumerate(model.simulations[B].ρ_history[::5,:]):
        rho = rho.reshape(1,-1)
        plt.plot(model.x[0], pg.Ex(model, rho, B)[0], label=i*5)
    if legend:
      plt.legend()
    plt.show()
    # Plot History of ρ
    for i, rho in enumerate(model.simulations[B].ρ_history[::5,:]):
        #print(rho.shape)
        plt.plot(model.x[0], rho, label=i*5)
    if legend:
        plt.legend()
    plt.show()

def plot_energy_over_iterations_fig(model, B):
    '''
    Figure with a plot showing the total energy density (value of the Hall functional) over the iterations.
    The figure is returned.
    '''
    fig, ax1 = plt.subplots(1, figsize = (5.7, 0.5*3.523), layout='constrained')

    simulation = model.simulations[B]
    ax1 = plot_total_energy_density(model, simulation, ax1)
    ax1.set_subfig_label(f'plt:B{B*B_conv_SI:3.2f}-energy_over_iterations', vpos='top', hpos='right')
    return fig

def plot_density_potentials(model, B, Vtot_LandauL=0, y_zoom=2e-2):
    '''
    Figure with two subplot showing the minimizing density and the total potential with either its components if Vtot_LandauL=0 or the given number of Landau levels.
    For the latter the zoom may be necessary (and autoscaling is complicated).
    The figure is returned.
    '''
    fig, (ax1, ax2) = plt.subplots(2, figsize = (5.7, 1.25*3.523), height_ratios=[1, 2], sharex=True, layout='constrained')

    simulation = model.simulations[B]
    ax1 = plot_density(model, simulation, ax1, donors=False)
    if Vtot_LandauL==0:
        ax2 = plot_potential_energies(model, simulation, ax2)
    else:
        ax2 = plot_tot_potential_Landau_levels(model, simulation, ax2, Landau_levels=Vtot_LandauL, y_zoom=y_zoom)
    
    ax1.xaxis.label.set_visible(False)
    ax1.set_subfig_label(f'plt:B{B*B_conv_SI:3.2f}-density', vpos='top', hpos='right')
    ax2.legend(numpoints=10)
    ax2.set_subfig_label(f'plt:B{B*B_conv_SI:3.2f}-potential_components_gradient', vpos='bottom', hpos='right')
    return fig

def plot_density_fillingfactor(model, CSG=False, title=True, subfig_label='several_B_values'):
    '''
    For all B in model.simulations, the density and filling factor are plotted in a right and left subplot, respectively.
    The figure is returned.
    '''
    num_lines = len(model.simulations)
    #plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.viridis(np.linspace(0,1,num_lines)))
    cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
    cmap = mpl.colors.LinearSegmentedColormap.from_list('my_cmap', colors=[cycle[0],cycle[1],cycle[3]])

    with mpl.rc_context({'axes.prop_cycle': plt.cycler("color", cmap(np.linspace(0,1,num_lines)))}):
        fig, [ax1, ax2] = plt.subplots(1, 2, figsize=(5.7, 3.523), layout='constrained')

        for B, simulation in model.simulations.items():
            ax1, line = plot_fillingfactor(model, simulation, ax1, label=r'\qty{{{:.3g}}}{{\tesla}}'.format(B*B_conv_SI))
            ax2 = plot_density(model, simulation, ax2, label=r'\qty{{{:.3g}}}{{\tesla}}'.format(B*B_conv_SI), donors=False, plot_kwargs={'color':line.get_color()})
            if B != 0 and CSG:
              plot_CSG_xk_ak(model, simulation, ax1, color=line.get_color())
            #   plot_CSG_xk_ak(model, simulation, ax2, ρ_bool=True, color=line.get_color())
        if CSG:
            ax2 = plot_CSG_density(model, simulation, ax2, n0_ρ=model.n0, LG=False)

        ax1.set_xlim(left=-1, right=0)
        ax2.set_xlim(left=0, right=1)
        # ax2.set_xlim(left=-1, right=0)
        ax2.legend()
        ax1.set_subfig_label('plt:filling_factors-' + subfig_label, vpos='top', hpos='left')
        ax2.set_subfig_label('plt:densities-' + subfig_label, vpos='top', hpos='right')
        if title:
            fig = plotTitleModel(model, fig)
    # Set to default color cycle: plt.rcParams["axes.prop_cycle"] =  'Vega' and 'd3' # See http://w.9lo.lublin.pl/DOC/python-matplotlib-doc/html/users/dflt_style_changes.html#colors-in-default-property-cycle
    return fig

# Plots for V_ext
def plot_Vext_densities_potentials(models, B, y_zoom=2e-2):
    '''
    Figure with three subplots to compare several models with different external potentials (but equivalent in the other model parameters).
    The first shows the densities.
    The second and third subplots show the total potentials, external potentials, and chemical potentials. Where the third subplot is zoomed closely to the chemical potential.
    The figure is returned.
    '''
    fig, (ax1, ax2, ax3) = plt.subplots(3, figsize = (5.7, 1.57*3.523), height_ratios=[2, 3, 3], sharex=True, layout='constrained')

    for model in models.values():
        simulation = model.simulations[B]
        label=r'$\Vsub{{L}} = \num{{{:.4g}}}\,E_0$'.format(model.V_ext/model.E0)
        line_d = ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label)
        color = line_d[0].get_color()
        for ax in [ax2, ax3]:
            ax.plot(model.xpd[0], model.V_donor[0] + model.V_K_1δ_lin(simulation.ρ_last)[0] + model.V_external[0], color=color, label=r'$\Vsub{tot}\,$(' + label +r')')
    ax2.plot(model.xpd[0], model.V_external[0], c=color, ls=':', label=r'$\Vsub{G}$') # c='C7'
    for ax in [ax2, ax3]: # same B, hence same μ and one line suffices
        ax.plot(model.xpd[0], np.ones_like(model.x[0])*simulation.μ, c='C3', ls='--', label=r'$\mu \,(\mu_\mathup{{G}} = \num{{{:.4g}}}\,E_0)$'.format(model.μ_G/model.E0))

    ax1.set_ylabel(r'density in $\unit{\per\square\cm}$')
    # ax1.set_subfig_label(f'plt:B{B*B_conv_SI:3.2f}-density', vpos='top', hpos='right')
    axis_energy_per_lengthPower(ax2, model, r'potential energy $V$', lengthPower=0)
    axis_energy_per_lengthPower(ax3, model, r'potential energy $\Vsub{tot}$', lengthPower=0)
    ax3.set_xlabel(r'$x/d$')
    if y_zoom != None:
        ax3.set_ylim(top=simulation.μ * (1 - y_zoom), bottom=simulation.μ* (1 + y_zoom))
    ax1.legend()
    ax2.legend(bbox_to_anchor=(0.5, 0.7), loc='upper center')
    # ax3.legend()
    ax1.set_subfig_label(f'plt:Vext_01E0-B{B*B_conv_SI:3.2f}-densities', vpos='top', hpos='right')
    ax2.set_subfig_label(f'plt:Vext_01E0-B{B*B_conv_SI:3.2f}-potentials', vpos='top', hpos='right')
    ax3.set_subfig_label(f'plt:Vext_01E0-B{B*B_conv_SI:3.2f}-potentials-zoom', vpos='bottom', hpos='right')
    return fig

# Depreciated version
def plot_densities_tot_pot_energies_Vext(models, B, zoom=0.01):
    '''
    Figure with two subplots to compare several models with different external potentials (but equivalent in the other model parameters).
    The first shows the densities.
    The second subplot shows the total potentials and external potentials.
    The figure is returned.
    '''
    fig, (ax1, ax2) = plt.subplots(2, figsize = (5.7, 1.5*3.523), height_ratios=[3,2], sharex=True, layout='constrained')

    for model in models.values():
        simulation = model.simulations[B]
        label=r'$\Vsub{{G}} \text{{with}} \Vsub{{L}} = \num{{{:.4g}}}\,E_0$'.format(model.V_ext/model.E0)
        # label=r'$\rho_{{\mu = \qty{{{:.2g}}}{{\J}} }}$'.format(model.μ_G*V_conv_SI)

        line_d = ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label)
        # ax1 = plot_CSG_density(model, simulation, ax1, n0_ρ=model.n0, LG=False)
        # ax1 = plot_CMS_density(model, simulation, ax1, n0_ρ=model.n0)

        color = line_d[0].get_color()
        ax2.plot(model.xpd[0], model.V_K_1δ_lin(simulation.ρ_last)[0] + model.V_donor[0], color=color, label=r'$\Vsub{H} + \Vsub{D}$')
        ax2.plot(model.xpd[0], model.V_external[0], color='k', label=r'$\Vsub{ext}$')
        ax2.plot(model.xpd[0], model.V_K_1δ_lin(simulation.ρ_last)[0] + model.V_donor[0] + model.V_external[0], color='red', label=r'$\Vsub{tot}$')
        # ax2.plot(model.xpd[0], model.djB(simulation.ρ_last, simulation.B)[0] -model.V_donor[0] + model.V_external[0], color=color, label=r'$\Vsub{tot}$')
        # ax2.plot(model.xpd[0], model.grad_func(simulation.ρ_last, simulation.B)[0], color=color, label=r'$\Vsub{tot}$')
    ax2.plot(model.xpd[0], np.ones_like(model.x[0])*simulation.μ, ls='--', color='grey', label=r'$\mu = \mu_\mathup{{G}} = \num{{{:.4g}}}\,E_0$'.format(simulation.μ/model.E0))
    
    ax2.set_ylim(top=simulation.μ * (1 - zoom), bottom=simulation.μ * (1 + zoom))
    ax1.set_ylabel(r'density in $\unit{\per\square\cm}$')
    axis_energy_per_lengthPower(ax2, model, r'potential energy $\Vsub{tot}$', lengthPower=0)
    ax2.set_xlabel(r'$x/d$')
    ax1.legend()
    return fig


# Plots for several models and their simulation[B=0]
def plot_densities_pot_energies_compare(models):
    '''
    Figure with two subplots to compare several models with different chemical potentials (but equivalent in the other model parameters) with the densities from closed formulas by CSG and CMS.
    The first shows the densities and also CSG- and CMS-densities.
    The second subplot shows the total potentials and chemical potentials.
    The figure is returned.
    '''
    fig, (ax1, ax2) = plt.subplots(2, figsize = (5.7, 1.5*3.523), height_ratios=[3,2], sharex=True, layout='constrained')

    for model in models.values():
        simulation = model.simulations[0]
        label=r'$\muG = \num{{{:.4g}}}\,E_0 = \qty{{{:.2g}}}{{\eV}}$'.format(model.μ_G/model.E0, model.μ_G*V_conv_SI)
        # label=r'$\rho_{{\mu = \qty{{{:.2g}}}{{\J}} }}$'.format(model.μ_G*V_conv_SI)

        line_d = ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label)
        ax1 = plot_CSG_density(model, simulation, ax1, n0_ρ=model.n0, LG=False)
        ax1 = plot_CMS_density(model, simulation, ax1, n0_ρ=model.n0)

        color = line_d[0].get_color()
        ax2.plot(model.xpd[0], model.V_donor[0] + model.V_K_1δ_lin(simulation.ρ_last)[0] + model.V_external[0], color=color, label=r'$\Vsub{tot}$')
        # ax2.plot(model.xpd[0], model.grad_func(simulation.ρ_last, simulation.B)[0], color=color, label=r'$∇\cEHall$')
        ax2.plot(model.xpd[0], np.ones_like(model.x[0])*simulation.μ, ls='--', color=color, label=r'$\mu = \mu_\mathup{{G}} = \num{{{:.4g}}}\,E_0$'.format(simulation.μ/model.E0)) # for B=0: μ = μ_G
    
    ax1.plot(model.xpd[0], np.ones_like(model.x[0])*model.n0, ls='-.', label=r'donors')
    ax1.set_ylabel(r'density in $\unit{\per\square\cm}$')
    axis_energy_per_lengthPower(ax2, model, r'potential energy $\Vsub{tot}$', lengthPower=0)
    ax2.set_xlabel(r'$x/d$')
    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend([handles[i] for i in [9,0,3,6,7,8]], [labels[i] for i in [9,0,3,6,7,8]], bbox_to_anchor=(0.5, 0.04), loc='lower center')
    # ax2.legend(bbox_to_anchor=(0.5, 0.04), loc='lower center')
    ax1.set_subfig_label(f'plt:diff-mu-B0-compare-Gerhardts-densities', vpos='top', hpos='right')
    ax2.set_subfig_label(f'plt:diff-mu-B0-compare-Gerhardts-tot_potentials', vpos='bottom', hpos='right')
    plt.show()
    return fig

def plot_densities_edge_compare(models, B, x_range=0.05, xlim=None):
    '''
    Figure to compare several models with different chemical potentials (but equivalent in the other model parameters) with the CSG- and CMS-densities, and the Lier and Gerhardts edges.
    The figure focusses on the edge of the plots.
    The figure is returned.
    '''
    fig, ax1 = plt.subplots(1, figsize = (5.7, 3.523), sharex=True, layout='constrained')

    if xlim !=None:
      ax1.set_xlim(right=xlim)
    else:
      model = list(models.values())[1]
      xl_i = model.simulations[B].xl_i
      x_range_i = np.rint(x_range*2*model.d/model.δ).astype(int)
      ax1.set_xlim(left=model.xpd[0,max(xl_i - x_range_i, 0)], right=model.xpd[0, xl_i + x_range_i])


    for model in models.values():
        simulation = model.simulations[B]
        # label=r'$\rho_{{\mu = \qty{{{:.2g}}}{{\J}} }}$'.format(model.μ_G*V_conv_SI)
        label=r'$\muG = \num{{{:.4g}}}\,E_0$'.format(model.μ_G/model.E0)

        ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label)
        ax1 = plot_CSG_density(model, simulation, ax1, n0_ρ=model.n0, LG=True)
        ax1 = plot_CMS_density(model, simulation, ax1, n0_ρ=model.n0)
    
    ax1.set_xlabel(r'$x/d$')
    ax1.set_ylabel(r'density in $\unit{\per\square\cm}$')
    
    ax1.plot([],[], color='none', label=' ')
    handles, labels = ax1.get_legend_handles_labels()
    handles_ind = 4*np.arange(len(models.keys()))
    handles_ind = np.append(handles_ind, [4,3,1,2] + handles_ind[-1])
    #handles_ind = np.append(handles_ind, np.arange(1,4) + handles_ind[-1])
    # ax1.legend([handles[i] for i in handles_ind], [labels[i] for i in handles_ind], bbox_to_anchor=(0.5, 0.06), loc='lower left')
    legend = ax1.legend([handles[i] for i in handles_ind], [labels[i] for i in handles_ind], bbox_to_anchor=(0, -0.1, 1, -0.1), loc='upper left', ncols=3, mode="expand", borderaxespad=0.)
    # fig.legend([handles[i] for i in handles_ind], [labels[i] for i in handles_ind], bbox_to_anchor=(0, 0, 1, 0), loc='upper left', ncols=3, mode="expand", borderaxespad=0.)
    plt.show()
    return fig

def _plot_densities_edge_compare_classic(ax1, models, B, B0=False):
    if B0:
      for model in models.values():
        simulation = model.simulations[0]
        label=r'$\rho_{{\mu = \qty{{{:.2g}}}{{\J}}, B = \qty{{0}}{{\tesla}} }}$'.format(model.μ_G*V_conv_SI)
        ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label, ls='--', alpha=0.8)
      ax1.set_prop_cycle(None)

    for model in models.values():
        simulation = model.simulations[B]
        # label=r'$\rho_{{\mu = \qty{{{:.2g}}}{{\J}} }}$'.format(model.μ_G*V_conv_SI)
        label=r'$\muG = \num{{{:.4g}}}\,E_0$'.format(model.μ_G/model.E0)

        ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label)
        ax1 = plot_CSG_density(model, simulation, ax1, n0_ρ=model.n0, LG=False)
        ax1 = plot_CMS_density(model, simulation, ax1, n0_ρ=model.n0)
    
    return ax1

def plot_densities_edge_compare_classic(models, B, x_range=0.05, xlim=None, B0=False, inset=False):
    '''
    Figure to compares several models with different chemical potentials but for a finite magnetic field (rather high so we are in the classical limit case)
    Again we compare with the CSG- and CMS-densities, and the Lier and Gerhardts edges.
    
    B0: Plots the densities for B=0 of each model with a dashed line and lower opacity.
    inset: Enlarges the edge in an inset figure
    x_range: gives the enlargement
    xlim: of the large plot needs to be given (e.g. xlim={'left':-1.05, 'right': 0} )

    The figure is returned.
    '''
    fig, ax1 = plt.subplots(1, figsize = (5.7, 1.4*3.523), layout='constrained')

    if xlim !=None:
      ax1.set_xlim(**xlim)
    else:
      #if xlim == None:
      model = list(models.values())[1]
      xl_i = model.simulations[B].xl_i
      x_range_i = np.rint(x_range*2*model.d/model.δ).astype(int)
      ax1.set_xlim(left=model.xpd[0,max(xl_i - x_range_i, 0)], right=model.xpd[0, xl_i + x_range_i])

    ax1 = _plot_densities_edge_compare_classic(ax1, models, B, B0=True)
    ax1.set_xlabel(r'$x/d$')
    ax1.set_ylabel(r'density in $\unit{\per\square\cm}$')
    ax1_ylim = ax1.get_ylim()

    if inset:
        axins = ax1.inset_axes([0.4, 0.035, 0.57, 0.55])
        axins = _plot_densities_edge_compare_classic(axins, models, B, B0=True)
        model = list(models.values())[1]
        xl_i = model.simulations[B].xl_i
        x_range_i = np.rint(x_range*2*model.d/model.δ).astype(int)
        axins.set_xlim(left=-1.01, right=model.xpd[0, xl_i + x_range_i])
        axins.set_ylim(top=1e11, bottom=-0.05e11)
        axins.set_xticklabels([])
        axins.set_yticklabels([])

        ax1.indicate_inset_zoom(axins, edgecolor="black")

    ax1.plot([],[], color='none', label=' ')
    handles_ind = 3*np.arange(len(models.keys()))
    handles_ind = np.append(handles_ind, [3,1,2] + handles_ind[-1])
    handles_ind = handles_ind + len(models.keys()) # Since B=0 is plotted on ax1
    # if B0:
        # handles_ind = np.append(handles_ind + len(models.keys()), np.arange(len(models.keys())))

    # if B0:
    #   ax1.set_prop_cycle(None)
    #   for model in models.values():
    #     simulation = model.simulations[0]
    #     label=r'$\rho_{{\mu = \qty{{{:.2g}}}{{\J}}, B = \qty{{0}}{{\tesla}} }}$'.format(model.μ_G*V_conv_SI)
    #     ax1.plot(model.xpd[0], simulation.ρ_last[0], label=label, ls='--')
    #   handles_ind =np.append(handles_ind, np.arange(len(models.keys())) + 1 + handles_ind[-1])

    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend([handles[i] for i in handles_ind], [labels[i] for i in handles_ind], bbox_to_anchor=(0, -0.07, 1, -0.07), loc='upper left', ncols=3, mode="expand", borderaxespad=0.)
    ax1.set_ylim(ax1_ylim)
    plt.show()
    return fig


## CSG Results
def plot_CSG_density(model, simulation, ax, n0_ρ=0, LG=True):
    '''
    Plot CSG density onto given axes axes ax and return it.
    '''
    if n0_ρ == 0:
        n0_ρ = simulation.ρ_last.max()
    ρ_CSG = np.zeros_like(model.x)
    # left edge
    V_g = (model.V_ext - simulation.μ)/e
    l_CSG = V_g*2*model.κ / (4*np.pi**2*model.n0*e)
    xC = model.x + model.d - l_CSG
    xl = xC[:,0:model.i_mid]
    mask = np.where((xl - l_CSG) > 0)
    ρ_CSG[mask] = np.sqrt((xl[mask] - l_CSG)/(xl[mask] + l_CSG)) * n0_ρ
    # right edge
    V_g = - simulation.μ/e
    l_CSG = V_g*2*model.κ / (4*np.pi**2*model.n0*e)
    xC = -model.x + model.d - l_CSG
    xr = xC[:,model.i_mid:]
    mask = np.where((xr - l_CSG) > 0)
    ρ_CSG[(mask[0], mask[1] + model.i_mid)] = np.sqrt((xr[mask] - l_CSG)/(xr[mask] + l_CSG)) * n0_ρ

    ax.plot(model.xpd[0], ρ_CSG[0], 'k:', label=r'CSG')

    if LG:
        a0_LG = model.κ*hbar**2/(2*e**2*model.m)
        xl_LG = -model.d + (2*l_CSG - 2*a0_LG)
        xr_LG = model.d - (2*l_CSG - 2*a0_LG)
        ax.plot([xl_LG/model.d, xr_LG/model.d], [0, 0], color='k', marker='|', markersize=10, label=r'LG', ls='None')

    return ax

def plot_CSG_xk_ak(model, simulation, ax, ρ_bool=False, color='grey'):
    '''
    Plots position of incompressible strips calculated by CSG formulas onto given axes axes ax and return it.
    '''
    # left edge
    V_g = (model.V_ext - model.μ_G)/e
    l_CSG = V_g*2*model.κ / (4*np.pi**2*model.n0*e)
    aB_CSG = hbar**2*model.κ/(model.m*e**2)
    xC = model.x + model.d - l_CSG
    B = simulation.B
    ν0 = model.n0/model.dL(B) # CSG densities with n0 agree better
    ν0_ρ = simulation.ρ_last.max()/model.dL(B)
    k = np.arange(1, np.floor(ν0_ρ) + 1, step=1) # include 1 if ν0 = 1
    xk = l_CSG * (2 * ν0**2)/(ν0**2 - k**2) - model.d
    ak = 4* np.sqrt((aB_CSG*l_CSG*k)/np.pi)*ν0/(ν0**2 - k**2)
    if ρ_bool:# if plotted in density axis and not filling factor axis
        k = k*model.dL(B)
    # color=next(ax._get_lines.prop_cycler)['color']
    # color='grey'
    # ax.plot(xk/model.d, k, marker='x', color='grey', ls='None')
    # ax.plot(xk - ak/2, k, markers='.', color=color)
    # ax.plot(xk + ak/2, k, markers='.', color=color)
    for i, cs in enumerate(zip((xk - ak/2)/model.d, (xk + ak/2)/model.d)):
        ax.plot(cs, k[i]*np.array([1,1]), ls='--', marker='x', color='grey', alpha=0.75, markeredgecolor=color) # label=r'IS_{CSG} $\ny={}$'.format(k[i])
    
    return ax

def CMS_elliptic_int_b(m):
    '''
    Auxilary function to solve equation (7) from CMS numerically to obtain b. Note, m = b/d.
    '''
    return ellipe(1-m**2) - m**2 * ellipk(1-m**2)

def CMS_b(model, simulation):
    '''
    Solve equation (7) from CMS numerically to obtain (and return) b.
    '''
    V_G = (model.V_ext - simulation.μ)/e
    bpd = newton(lambda m: CMS_elliptic_int_b(m) - (V_G*2*model.κ)/(4*np.pi*e*model.n0*model.d), 0.5)
    b = bpd * model.d
    return b

def plot_CMS_density(model, simulation, ax, n0_ρ=0):
    '''
    Plot CMS density onto given axes axes ax and return it.
    '''
    if n0_ρ ==0:
        n0_ρ = simulation.ρ_last.max()
    ρ_CMS = np.zeros_like(model.x)
    b2mx2 = ((CMS_b(model, simulation))**2 - model.x**2)
    mask = np.where(b2mx2 > 0)
    ρ_CMS[mask] = np.sqrt(b2mx2[mask]/(model.d**2 - model.x[mask]**2)) * n0_ρ
    ax.plot(model.xpd[0], ρ_CMS[0], 'k', dashes=[4,4], label=r'CMS')
    return ax


# Plot of j_B and j_0
def plot_jB_j0(model, B, title=True):
    '''
    Figure showing jB and j0 for magnetic field B. (Actually, the choice of B is not so important and the model is just needed for some model parameters.)
    The figure is returned.
    '''
    dL = model.dL(B)
    p = dL*np.arange(0, 5.1, 0.01).reshape([1,-1])

    fig,ax1 = plt.subplots(1)
    ax1.plot(p[0], model.jB(p, B)[0], label=r'$j_B$')
    ax1.plot(p[0], model.jB(p, 0)[0], label=r'$j_0$')

    ax1.set_xticks([dL*i for i in range(6)], [f'{i:d}' + r'$\,\frac{e}{\hbar c} \frac{B}{\pi}$' for i in range(6)])
    ax1.set_yticks([dL*model.hbar_ωc(B)*(i**2-i)/2 for i in range(6)], ['{: .0f}'.format((i**2-i)/2) + r'$\,\frac{e^2}{m_* c^2} \frac{B^2}{\pi}$' for i in range(6)])

    ax1.set_xlabel(r'$p$ / $\unit{\per\square\cm}$')
    ax1.set_ylabel(r'Kinetic energy density function $j_B$') # $\unit{{\J\per\square\cm}}$
    ax1.legend()
    if title:
        ax1.set_title(r'Kinetic energy density function $j_B$')

    return fig

def plot_djB(model, B, title=True):
    '''
    Figure showing djB for magnetic field B. (Actually, the choice of B is not so important and the model is just needed for some model parameters.)
    The figure is returned.
    '''
    dL = model.dL(B)
    p = dL*np.arange(0, 5.1, 0.01).reshape([1,-1])

    fig,ax1 = plt.subplots(1)
    ax1.plot(p[0], model.djB(p, B)[0], label=r'$dj_B/d\rho$')
    ax1.plot(p[0], model.djB(p, 0)[0], label=r'$dj_0/d\rho$')
    ax1.plot(p[0], model.djB_smooth(p, B, k)[0],ls=':', label=r'$dj_B^{\mathup{smooth}}/d\rho$')

    ax1.set_xticks([dL*i for i in range(6)], [f'{i:d}' + r'$\,\frac{e}{\hbar c} \frac{B}{\pi}$' for i in range(6)])
    ax1.set_yticks([model.hbar_ωc(B)*i for i in range(6)], ['{: .0f}'.format(i) + r'$\,\hbar \omega_c$' for i in range(6)])

    ax1.set_xlabel(r'$p$ / $\unit{\per\square\cm}$')
    ax1.set_ylabel(r'Kinetic energy per electron in $\unit{{\J}}$') # $\unit{{\J}}$
    ax1.legend()
    if title:
        ax1.set_title(r'Kinetic energy per electron $dj_B/d\rho$')

    return fig

