# Comments for someone aiming to work with this file:
# This is a file with code used to calculate the simulations (perform the minimization) for different model parameters and gradient descent methods. I worked with an interactive Jupyter interface in VSCodium. Therefore, this code can't be used as a python script. 
# Actually, it may be easiest to integrate parts of this file in your own.

import datetime
import numpy as np
import dill
from importlib import reload
# import scipy.integrate as integrate
# import scipy.constants as constants
import matplotlib as mpl
import matplotlib.pyplot as plt
# # Define plot style for the thesis
# plt.style.use('Thesis-plot_style.mplstyle')
# zelf_defined = '' # File with self defined Latex-commands used in the labels and titles of the plots
# with open('../Thesis/Zelf-defined-Commands.tex', 'r') as f:
#     zelf_defined = f.read()
# preamble = r'''
#     \usepackage{siunitx}
#     \usepackage{mathtools}
#     \newcommand\phantomsubcaption{} % for subcaptions for each subfigure
#     \newcommand\subcaptiontext[1]{(a)} % for subcaptions for each subfigure
#     \newcommand\subfigcaptionlabel[1]{(a)} % for subcaptions for each subfigure
#     \newcommand{\LTadd}[1]{}
#     \newcommand{\LTalter}[2]{#1}
#     \newcommand{\LTskip}[1]{#1}
#     \newcommand{\LTinput}[1]{}
# ''' + zelf_defined
# from matplotlib.backends.backend_pgf import PdfPages
# # plt.rc('text', usetex=True)
# plt.rc('text.latex', preamble= r'\newcommand{\mathup}[1]{\mathrm{#1}}' + preamble)
# # plt.rcParams['lines.linewidth'] = 0.93

# from matplotlib.backends.backend_pgf import FigureCanvasPgf
# mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)
# # LaTeX preamble fürs pgf backend.  Nicht `text.usetex: True`, dann
# # werden die Graphen sogar schneller angezeigt
# plt.rcParams.update({
#     "pgf.preamble": r'\usepackage{unicode-math}' + r'\usepackage[defaultfeatures={Ligatures={TeX,Common,Rare}}]{libertinus-otf}' + preamble,
#     # 'pgf.rcfonts': True,
#     # 'font.family': 'serif'
# })
# # plt.rc(text.path_math = False)

from cycler import cycler
plt.rcParams['axes.prop_cycle'] = cycler(
    color=[
        # xcolor rgb
        '#0026A6',  # blue
        '#8C0033',  # red
        '#008000',  # green
        # '#99994D',  # gold
        # '#002626',  # anthrazit
        '#CC8000',  # ochre

        '#0099E5',  # mediumblue
        '#CC3333',  # orangered
        # '#33CCCC',  # lightblue
        # '#66E580',  # turquoise
        '#4DCC00',  # mediumgreen
        '#9933CC',  # pink
        # '#998080',  # lightbrown
        # '#CCB259',  # sand
        # '#663300',  # brown
        ]
)

import Functions_GradientDescents as gd
import Functions_Potentials_Equal as pe
unit_sys = 'gaussian' # e.g.: ones, gaussian

if unit_sys == 'ones':
    from Constants_Ones import * # better: import ... as consts
    import Functions_Potentials_Gaussian as pg # Should be changed to different file
elif unit_sys == 'gaussian':
    from Constants_Gaussian import * # better: import ... as consts
    import Functions_Potentials_Gaussian as pg
else:
    print('No unit system choosen!')

import Functions_Plotting as mplt

if unit_sys == 'ones':
    d = 1
    δ = 0.01
    N = int(2*d/δ) + 1
    δ = 2*d/(N-1)
    
    n0 = 2
    ne0 = 0.5
    E0 = 1
    a0 = 1
    D0 = 1
    V_ext = 0
elif unit_sys == 'gaussian':
    d = 1.5e-4 # in cm
    δ = 0.001e-4 # in cm
    N = int(2*d/δ) + 1
    δ = 2*d/(N-1)
    
    n0 = 4e11 # in 1/cm²
    E0 = 4.377*e_SI * 1e7 # in erg; eV = {e} C V = {e} J = {e} 10⁷ erg
    a0 = 9.79e-7/2 # in cm
    D0 = 2.79/e_SI * 1e6 # in 1/(erg cm²); 10⁶/{e} 1/(erg cm²) = 10¹⁰/(meV * cm²)
    
    # V_ext = E0/10
    V_ext = 0
    ne0 = n0

    # B = 1*1e4 # in G; T = 10⁴G

κ = 2*np.pi*e**2*n0*d/E0
print(κ*4*np.pi*ε0_SI/100) # C/mV (factors to convert to SI-units)
κ = 2*a0*e**2*np.pi*D0
print(κ*4*np.pi*ε0_SI/100) # C/mV (factors to convert to SI-units)
m = D0 * hbar**2 * np.pi
print(m/1000) # kg
m = np.pi*n0*d*hbar**2/(a0*E0)
print(m/1000) # kg
constants.m_e

x = np.linspace(-d, d, N).reshape(1,-1)
ρ = np.ones_like(x)*ne0
ρ[0,0] = 0
ρ[0,-1] = 0

# V_ext = 0
model_one = pg.Models(x=x, d=d, δ=δ, m=m, n0=n0, κ=κ, V_ext=V_ext, name_obj_func=pg.E, name_grad_func=pg.gradE, E0=E0, xpd=x/d)

simulation0 = gd.Simulations(model_one, B=0)

# # Gradient Descent with Energy-Functional
# B = 0

# # Without smoothing
# simulation0.gradient_descent_adj_trap(model_one, 1e3, 1e-10, ρ, learning_rate=1e10, adjust_learning_rate=1, momentum=0.9)

# # With smoothing
# simulation0.lB = 0
# simulation0.gradient_descent_adj_smooth_trap(model_one, 1e3, 1e-10, ρ, learning_rate=1e10, adjust_learning_rate=1, momentum=0.9)

# With chemical potential
learning_rate = np.abs(np.average(ρ[:,1:-1]/model_one.grad_func(ρ, simulation0.B)[:,1:-1]))*1e-2
model_one.μ_G = -0.1598*model_one.E0 # -0.3052*model_one.E0, -0.1598*model_one.E0; -2.5e-13, -2.5e-12, model_one.hbar_ωc(B)/2
simulation0.μ = model_one.μ_G - model_one.hbar_ωc(simulation0.B)/2
#simulation0.μ = model_one.obj_func(ρ, simulation0.B)/np.trapz(ρ[0], dx=δ)
simulation0.gradient_descent_adj_chemicalpot(model_one, 1e3, 1e-12, ρ, learning_rate=learning_rate, adjust_learning_rate=1, momentum=0.6, adjust_momentum=1)

print('iterations: ', simulation0.iterations)

B=0
## Plot History of E[ρ](x)
mplt.plotHistory(model_one, B, legend=False)

## Plot data for specific B
fig = mplt.plotEnergyDensityPotentials(model_one, 0)
plt.show()

simulation0.edge_points()
print(x[0, np.array([simulation0.xl_i, int(simulation0.xm_i), simulation0.xr_i])]/d)

zoom = 0.01
fig = mplt.plot_density_edge(model_one, 0, x_range=0.1, ylim= {'top': simulation0.μ/E0 * np.trapz(simulation0.ρ_last, dx=model_one.δ) * (1 - zoom), 'bottom': simulation0.μ/E0 * np.trapz(simulation0.ρ_last, dx=model_one.δ) * (1 + zoom)})
plt.show()

# with open('Plots/Edges-8-9-compare-Gerhardts.pkl', 'wb') as f:
#     dill.dump(models, f)
##########################################################################################################################
## Plots for several B-values and saving
# model = pg.Models(x=x, d=d, δ=δ, m=m, n0=n0, κ=κ, V_ext=V_ext, name_obj_func=pg.E, name_grad_func=pg.gradE_smooth, E0=E0, xpd=x/d) # pg.gradE_smooth; model.k = 10000
# simulation = gd.Simulations(model, B=0)

# # Without smoothing
# simulation.gradient_descent_adj_trap(model, 1e3, 1e-10, ρ, learning_rate=0.1, adjust_learning_rate=1, momentum=0.8)
# # learning_rate=1e10, adjust_learning_rate=1, momentum=0.9

# # With smoothing
# model.lB = 0
# simulation.gradient_descent_adj_smooth_trap(model, 1e3, 1e-10, ρ, learning_rate=0.1, adjust_learning_rate=1, momentum=0.8)

# # With chemical potential
# learning_rate = np.abs(np.average(ρ[:,1:-1]/model.grad_func(ρ, simulation.B)[:,1:-1]))*1e-2
# model.μ_G = -0.1598*model.E0 # -0.3052*model.E0, -0.3071*model.E0, -0.1598*model.E0; -2.5e-13, -2.5e-12, model.hbar_ωc(B)/2
# simulation.μ = model.μ_G - model.hbar_ωc(simulation.B)/2
# simulation.gradient_descent_adj_chemicalpot(model, 1e3, 1e-12, ρ, learning_rate=learning_rate, adjust_learning_rate=1, momentum=0.6, adjust_momentum=1)

# # Calculate magnetic field for given filling factors
# ρ_last = model.simulations[0].ρ_last
# # ν = [3.1, 3, 2.9, 2.5, 2.1, 2, 1.9, 1.5, 1.1, 1, 0.9]
# # ν = [2.1, 2.01, 2, 1.9, 1.5, 1.1, 1.05, 1.01, 1, 0.9]
# ν = [2.1, 2.01, 2, 1.9, 1.5, 1.1, 1.01, 1, 0.9]
# ν.reverse()
# dν = ρ_last[0,int((N - 1)/2)]/ν
# B_list = c*h/(2*e) * dν

# learning_rate = np.abs(np.average(ρ[:,1:-1]/model.grad_func(ρ, simulation.B)[:,1:-1]))*1e-3
# ρ = ρ_last

# # ρ = simulation.ρ_last
# # [0.8,0.9,1,1.1,1.2,1.5]
# # np.linspace(1.5,1.8, 10)
# # [0.5, 0.31, 0.3, 0.28, 0.25, 0.16, 0.15, 0.14]
# for B in B_list:
#     # lB = None
#     # lB = np.sqrt(c*hbar/(e*B))/10
#     # lB = 0.05

#     simulation = gd.Simulations(model, B=B)

#     # simulation.gradient_descent_adj_trap(model, 1e3, 1e-10, model.simulations[0].ρ_last, learning_rate=0.1, adjust_learning_rate=1, momentum=0.8)

#     ## With chemical potential
#     simulation.μ = model.μ_G - model.hbar_ωc(simulation.B)/2
#     simulation.gradient_descent_adj_chemicalpot(model, 1e3, 1e-16, ρ, learning_rate=learning_rate, adjust_learning_rate=1, momentum=0.6)

#     # ## Smoothened Density Profiles
#     # # simulation.lB = np.sqrt(c*hbar/(e*B))/10
#     # simulation.lB = 0.05
#     # simulation.gradient_descent_adj_smooth_trap(model, 1e3, 1e-10, model.simulations[0].ρ_last, learning_rate=0.1, adjust_learning_rate=1, momentum=0.8)

#     # ## Shifted Gradient
#     # simulation.gradient_descent_adj_shifted_grad(model, 1e3, 1e-10, model.simulations[0].ρ_last, learning_rate=0.05, adjust_learning_rate=1, momentum=0.8)
    
#     # ## Projected Gradient
#     # simulation.gradient_descent_adj_projected_grad(model, 1e3, 1e-10, model.simulations[0].ρ_last, learning_rate=0.05, adjust_learning_rate=0.9, momentum=0.8)

#     print('B = ', B, ' done!')
#     print()


# # Plotting
# for B, simulation in model.simulations.items():
#     plt.figure()
#     plt.plot(simulation.func_history)
#     # plotHistory(model, B)
#     print('B = ', B, '; Iterations: ', simulation.iterations)

# # Plot all B-values
# for B in model.simulations.keys():
#     fig = mplt.plotEnergyDensityPotentials(model, B)
#     plt.show()

# for B in model.simulations.keys():
#     plotHistory(model, B)

# # Save as pdf
# # filename = 'Density-Potential-Chemical_Potential-fixed_value'
# filename = 'Different-Chemical_Potentials'
# filename = 'Chemical_Pot-μ_09-B_8_values'
# fig.savefig('Plots/' + filename + '.pdf')
# fig.savefig('../Thesis/Figures/' + filename + '.pgf')
# with PdfPages('Plots/'+filename+'.pdf') as pdf:
    
#     # for model in models.values():
#     #     for B in model.simulations.keys():
#     for B in model.simulations.keys():
#             fig = mplt.plotEnergyDensityPotentials(model, B)

#             pdf.savefig(fig)
#             plt.close()

#     # Set metadata of Pdf-file
#     pdfDic = pdf.infodict()
#     #pdfDic['Title'] = 'Multipage PDF Example'
#     pdfDic['Author'] = 'Maike Gnirß'
#     pdfDic['Subject'] = 'Density profiles with Gradient Descent'
#     #pdfDic['Keywords'] = 'PdfPages multipage keywords author title subject'
#     pdfDic['CreationDate'] = datetime.datetime.today()
#     # pdfDic['CreationDate'] = datetime.datetime(2022, 10, 26)
#     pdfDic['ModDate'] = datetime.datetime.today()

# # Save object
# # model = model_one
# with open('Plots/'+filename+'.pkl', 'wb') as f:
#     dill.dump(model, f)
#     # dill.dump(model_two, f)

# with open('Plots/Chemical_Potential-Zero.pkl', 'wb') as f:
    # dill.dump(models, f)

# # Load object
# with open('Plots/'+filename+'.pkl', 'rb') as f:
#     model_one = dill.load(f)
#     # model_two = dill.load(f)

########################################################################################################################
# Test V_K against V_{donor}
# ρ = np.ones_like(x)*model_one.n0
# ρ[0,0] = 0
# ρ[0,-1] = 0

# fig,ax = plt.subplots(1)
# ax1.plot(x[0], model.V_K_1δ_lin(ρ)[0], label='V_H')
# ax1.plot(x[0], model.V_donor[0], label='V_d')

# # Analyse V_H = V_K
# d = model_one.d
# xpd = x/d
# δpd = δ/d
# for ρ in simulation0.ρ_history[::100]:
#     ρ = ρ.reshape(1,-1)
#     plt.plot(x[0], -pe.V_K2_1δ_lin(xpd, ρ, δpd, d)[0], label='-V_K2')
#     plt.plot(x[0,1:-1], d*pe.V_K1_simps(xpd, ρ)[0], label='V_K1')
#     plt.plot(x[0], model_one.κ/(2*e**2)*model_one.V_K_1δ_lin(ρ)[0], label='V_H')
#     plt.legend()
#     plt.show()


# fig = plot_jB_j0(model_one, 1/10)
# plt.show()
# plt.savefig('Plots/jB.pdf')

