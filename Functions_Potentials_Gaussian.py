import numpy as np

import Functions_Potentials_Equal as pe
import Functions_GradientDescents as gd
from Constants_Gaussian import *
# from Constants_Ones import *

class Models: # Models(gd.BaseGradientDescents) BasePotentials in Functions_Potentials_Equal
    '''
    An instance of this class “Models” defines a model. The model object is initiated with all the model parameters and the corresponding donor and external potential is calculated and set as attributes.
    Another attribute is “simulations”, which is a dictionary collecting for different magnetic fields the corresponding simulation object (see the class “Simulations”).
    Methods of this class are the remaining electric potential, the Hartree potential (V_K), the kinetic energy density (jB) and the kinetic energy per electron (djB, djB_smooth)
    '''
    def __init__(self, **modelParas):
        for necessary_key in ['x', 'd', 'δ', 'm', 'n0', 'κ', 'V_ext', 'name_obj_func', 'name_grad_func']:
            if necessary_key not in modelParas.keys():
                print('The necessary model parameter', necessary_key, 'is missing.')
        self.__dict__.update(modelParas) # updates the list of attributes of the self. In case of overlapping keys the value of the latter dictionary is taken.

        self.simulations = {}
        self.i_mid = int((modelParas['x'].shape[1] - 1)/2)
        self.V_donor = self._calculate_V_donor()
        self.V_external = pe.V_external(self.x/self.d, self.V_ext)
    
    def _calculate_V_donor(self):
        '''
        Electric potential of the donors
        '''
        E0 = 2*np.pi*e**2*self.n0*self.d/self.κ
        # E0 = 4.377 # in eV
        U_d = -E0 * np.sqrt(1-(self.x/self.d)**2) # in eV
        return U_d

    def V_K_1δ_lin(self, ρ):
        '''
        Hartree potential,
        where the density is approximated to linear order and we seperated the integral in 1δ segments
        '''
        xpd = self.x/self.d
        δpd = self.δ/self.d
        # For both potential energies the integration is normalised (x/d), this leads to a prefactor of d. This is already included in the formula used in pe.V_K2_1δ_lin, but not in pe.V_K1_simps. Thus follow the prefactors below.
        VKx = pe.V_K2_1δ_lin(xpd, ρ, δpd)
        VKx[0,1:-1] = self.d*(pe.V_K1_simps(xpd, ρ) - VKx[0,1:-1])
        # # DELETE LATER:
        # print('This value should be 0: ',VKx[0,0])
        # print('This value should be 0: ',VKx[0,-1])
        # # End DELETE
        VKx[0,0] = 0
        VKx[0,-1] = 0
        return 2*e**2/self.κ*VKx


    # def dν(B, γ, ν):
    #     if isinstance(γ, np.int):
    #         if ν in np.arange(0,np.abs(γ)): # ν in {0,1,...,|γ|-1}
    #             return B/(2*np.pi)
    #         else:
    #             return B/np.pi
    #     else:
    #         return B/(2*np.pi)

    # Energy of the Landau levels in Gaussian units
    hbar_ωc = lambda self, B: hbar*e*B/(self.m*c) # Gaussian units
    # Degeneracy of the Landau levels
    dL = lambda self, B: (2*e*B)/(c*h) # Gaussian units

    def jB(self, ρ, B):
        '''
        Kinetic energy density

        Attention ν_m here is equal to (ν_max,paper - 1). Here ν_m is the highest Landau level that is occupied, not necessarily filled. (Note, that we start to fill the zeroth Landau level. Hence we round ν off.)
        jB(ρ) = Σ_{ν=0}^{ν_m - 1} ε_ν dL  +  (ρ - D_{ν_m - 1}) ε_{ν_m}
        '''
        if B == 0:
            return np.pi*hbar**2/(2*self.m)*ρ**2  # Gaussian units
            # return np.zeros_like(ρ)
        else:
            dL = self.dL(B)
            ν = ρ/dL
            ν_m = np.floor(ν) # ν_m described in function definition
            return self.hbar_ωc(B)*(-0.5*dL*(ν_m**2 + ν_m) + ρ*ν_m) # without 1/2 in the energy
            # return hbar_ωc*(-0.5*ν_m**2*dν + (ν_m + 0.5)*ρ) # with 1/2

    def djB(self, ρ, B):
        ''' Kinetic energy density per electron '''
        if B == 0:
            return np.pi*hbar**2/self.m*ρ
            # return np.zeros_like(ρ)
        else:
            dL = self.dL(B)
            ν = ρ/dL
            return self.hbar_ωc(B)*np.floor(ν) # (np.floor(ν) + 0.5) With 1/2

    def djB_smooth(self, ρ, B):
        '''
        Smooth version of the kinetic energy density per electron

        djB is a sum of Heaviside functions. Approximate the Heaviside functions with 1/(1 + exp{-2kx}) = 1/2 + 1/2 tanh(kx) and calculate the derivative.
        The approximation approaches djB for k → ∞.
        '''
        if B == 0:
            return np.pi*hbar**2/self.m*ρ
            # return np.zeros_like(ρ)
        else:
            dL = self.dL(B)
            ν = ρ/dL
            ν_mm = np.floor(ν).max()
            djB = 0.5 + 0.5*np.tanh(self.k*(ν - np.arange(1, ν_mm + 2).reshape(-1,1)))
            djB = self.hbar_ωc(B)*djB.sum(axis=0, keepdims=True) # without 1/2
            # djB = hbar*e*B/(m*c)*(djB.sum(axis=0, keepdims=True) + 0.5) # with 1/2
            return djB

    # def d2jB(ρ, B, m, k):
    #     '''
    #     Approximate the Heaviside functions in djB with 1/(1 + exp{-2kx}) and calculate the derivative.
    #     The approximation approaches djB and d2jB for k → ∞.
    #     '''
    #     if B == 0:
    #         return np.zeros_like(ρ)
    #     else:
    #         dν = c*h/(2*e*B)  # c*h/(2*e*B) in Gaussian units
    #         ν = ρ/dν
    #         ν_max = np.ceil(ν).max()
    #         d2jB = 0.5/np.cosh(k*(ν - np.arange(ν_max + 1).reshape(-1,1)))**2
    #         d2jB = hbar*e*B/(m*c)*k*d2jB.sum(axis=0, keepdims=True)
    #         return d2jB

    def obj_func(self, ρ, B):
        return self.name_obj_func(self, ρ, B)

    def grad_func(self, ρ, B):
        return self.name_grad_func(self, ρ, B)


def gradE(self, ρ, B):
    '''
    Gradient of the Hall functional without chemical potential
    '''
    return self.djB(ρ, B) + self.V_donor + self.V_K_1δ_lin(ρ) + self.V_external
    # return self.djB(ρ, B) + self.U_donor(x)/e + self.V_K_1δ_lin(x/self.d, ρ) - self.V_external
    
def gradE_smooth(self, ρ, B):
    '''
    Gradient of the Hall functional without chemical potential, but with the smoothened kinetic energy density per electron
    '''
    return self.djB_smooth(ρ, B) + self.V_donor + self.V_K_1δ_lin(ρ) + self.V_external
    # return self.djB_smooth(ρ, B, k) + self.U_donor(x)/e + self.V_K_1δ_lin(x/self.d, ρ)
    
def Ex(self, ρ, B):
    ''' E[ρ](x) Hall functional (energy functional) spatially resolved'''
    return self.jB(ρ, B) + ρ*( self.V_donor + 0.5*self.V_K_1δ_lin(ρ) + self.V_external )
    # return self.jB(ρ, B) + ρ*( self.U_donor(x) + e*self.V_K_1δ_lin(x/self.d, ρ) - e*V_external(x, self.V_ext) )
    
def E(self, ρ, B):
    '''
    Hall functional (energy functional)

    Integration implemented with trapezoidal rule:
    integrand = Ex(ρ, x, d, B, m, n0, κ, δ, δpd)[0]
    integrand[0] = integrand[0]/2
    integrand[-1] = integrand[-1]/2
    δ * np.sum(integrand)
    '''
    return np.trapz(Ex(self, ρ, B)[0], dx=self.δ)

